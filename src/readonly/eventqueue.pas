unit eventqueue;

interface

uses typecast;


type TEventQueue = class
  private
    
  
  protected
    debug:byte;
    TC:TTypeCast;
    id_read:longint;
    id_write:longint;
  
  public

    constructor Create();
    destructor Destroy();  
    
    function reset():boolean;
    function dump():boolean;
    function newStruct():TTypeCast;
    function freeStruct(var ts:TTypeCast):boolean;
    function addEvent(cmd:TTypeCast):boolean;
    function getNextEvent():TTypeCast;
end;

implementation


  constructor TEventQueue.Create();
  begin
    //inherited Create;
    debug:=0;
    TC:=TTypeCast.Create();
    id_read:=0;
    id_write:=0;
  end;

  destructor TEventQueue.Destroy();
  begin
    TC.Free();
    //writeln('CLEARED');
    inherited Destroy();
    //writeln('DESTROYED'); 
  end;


    function TEventQueue.dump():boolean;
    begin
      TC.dump();
    end;


    function TEventQueue.reset():boolean;
    begin
    
    end;

    function TEventQueue.newStruct():TTypeCast;
    begin
      result:=TTypeCast.create();
    
    end;
    
    function TEventQueue.freeStruct(var ts:TTypeCast):boolean;
    begin
      ts.free();
    end;
    
    function TEventQueue.addEvent(cmd:TTypeCast):boolean;
    begin
      TC.array_push(cmd);
      {
      TC[id_write].copy(cmd);
      
      inc(id_write);
    }
    end;
    
    function TEventQueue.getNextEvent():TTypeCast;
    var TT:TTypeCast=nil;
    begin
      TC.array_shift(TT);
      result:=TT;
      exit;
      if (TC.count() <= id_read) then 
      begin
        result:=nil;
        exit;
      end;
      TT:=TTypeCast.create();
      TT.copy(TC[id_read]);
      result:=TT;
      inc(id_read);    
    end;

end.
