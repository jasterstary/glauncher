unit GUI_elements;

{$mode objfpc} {$H+}

interface

uses
  Glib2
  ,Gdk2
  ,Gtk2
  ,Gtk2ext
  {unix,}
  ,pango
  ,gdk2pixbuf
  {,strings}
  ,sysutils
  {math,}
  {mysqldbase,}
  ,cfgfiles
  ,typecast;
  
//procedure gtk_button_set_always_show_image(button:PGtkButton; show:gboolean); cdecl; external gtklib;



type TGUIElements = class
  private
    TC:TTypeCast;
  
  protected
  
  public
  
    constructor Create();
    destructor Destroy();
    function Build(setup:TTypeCast):TTypeCast;
    function Window(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function TrayIcon(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function Box(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function Caption(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function Input(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function Button(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function ToggleButton(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function CheckButton(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function RadioButton(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function RadioButtonGroup(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;

    function ProgressBar(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;

    function Table(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function Text(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function Image(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function Select(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    function Notebook(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
    
    function NotebookAppendPages(setup, items:TTypeCast):boolean;

    function SetTableRows(setup, items:TTypeCast):boolean;
    function SetSelectRows(setup, items:TTypeCast):boolean;
    function SetValue(setup, items:TTypeCast):boolean;
    
    function GetValue(setup:TTypeCast):TTypeCast;
    
    function getStruct():TTypeCast;
    
    procedure sensing(setup:TTypeCast; b:boolean);
    procedure visible(setup:TTypeCast);

end;


function strpg(var pch:pGchar;ss:string):pGchar;

implementation

Type TByteArray = Array of Byte;


procedure gtk_status_icon_set_tooltip_text(el:pGTKStatusIcon; txt:pGchar); cdecl; external gtklib;

procedure gtk_cell_renderer_set_visible(cell:pGtkCellRenderer;visible:gboolean); cdecl; external gtklib;
//procedure gtk_text_buffer_get_iter_at_offset(buffer:PGtkTextBuffer; iter:PGtkTextIter; char_offset:gint); cdecl; external gtklib;
procedure gtk_window_set_deletable(w:pGTKWindow;b:gboolean); cdecl; external gtklib;

function strpg(var pch:pGchar;ss:string):pGchar;
begin
  pch:=StrAlloc(Length(ss)+1);
  pch:=strpcopy(pch,ss);
  result:=pch;
end;

  constructor TGUIElements.Create();
  begin
    //inherited Create;
    //debug:=0;
    TC:=TTypeCast.Create();
  end;

  destructor TGUIElements.Destroy();
  begin
    TC.Free();
    //writeln('CLEARED');
    inherited Destroy();
    //writeln('DESTROYED'); 
  end;

function TGUIElements.getStruct():TTypeCast;
begin
  result:=TC;
end;

function TGUIElements.Build(setup:TTypeCast):TTypeCast;
var row:TTypeCast=nil;
var inside:pointer;
begin
  setup.reset(row);
  while setup.each(row) do
  begin
    inside:=nil;
    if row['inside'].isString() and setup[row['inside']._str_]['el'].isPointer() then
      inside:=setup[row['inside']._str_]['el']._ptr_;
    if row['type'].isString() then
    begin
      //writeln(row['type']._str_);
      case row['type']._str_ of
        'window': begin
            setup[row._key_]['el']._ptr_:=window(inside, row);
          end;         
        'trayicon': begin
            setup[row._key_]['el']._ptr_:=TrayIcon(inside, row);
          end; 
        'box': begin
            setup[row._key_]['el']._ptr_:=box(inside, row);
          end;
        'caption': begin
            setup[row._key_]['el']._ptr_:=caption(inside, row);
          end;
        'input': begin
            setup[row._key_]['el']._ptr_:=input(inside, row);
          end;
        'button': begin
            //writeln('??',pchar(row['events']['click']['userdata']._ptr_^));
            setup[row._key_]['el']._ptr_:=button(inside, row);
          end;                
        'togglebutton': begin
            setup[row._key_]['el']._ptr_:=togglebutton(inside, row);
          end;
        'checkbutton': begin
            setup[row._key_]['el']._ptr_:=checkbutton(inside, row);
          end;
        'radiobutton': begin
            setup[row._key_]['el']._ptr_:=radiobutton(inside, row);
          end;
        'radiobuttongroup': begin
            setup[row._key_]['el']._ptr_:=radiobuttongroup(inside, row);
          end;
        'progressbar': begin
            setup[row._key_]['el']._ptr_:=progressbar(inside, row);
          end;
        'table': begin
            setup[row._key_]['el']._ptr_:=table(inside, row);
          end; 
        'text': begin
            setup[row._key_]['el']._ptr_:=text(inside, row);
          end;
        'image': begin
            setup[row._key_]['el']._ptr_:=image(inside, row);
          end;
        'select': begin
            setup[row._key_]['el']._ptr_:=select(inside, row);
          end;
        'notebook': begin
            setup[row._key_]['el']._ptr_:=notebook(inside, row);
          end;
      end;
      
      if setup[row._key_]['sensitive'].isBoolean() then sensing(setup[row._key_], setup[row._key_]['sensitive']._bool_);
    
    end;
  
  end;
  row.free();
  result:=setup;
  

end;

function TGUIElements.Window(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
begin

  if (not setup['width'].isInteger()) then setup['width']._int_:=500;
  if (not setup['height'].isInteger()) then setup['height']._int_:=500;
  //if (not setup['spacing'].isSet()) then setup['spacing']._int_:=3;
  //if (not setup['orientation'].isSet()) then setup['orientation']._str_:='horizontal';
  
  if setup['orientation']._str_='horizontal' then
  begin
    el := gtk_window_new (GTK_WINDOW_TOPLEVEL);
  end else begin
    el := gtk_window_new (GTK_WINDOW_TOPLEVEL);
  end;

  gtk_window_set_position(GTK_WINDOW (el),GTK_WIN_POS_CENTER);
  gtk_widget_set_size_request (GTK_WIDGET (el),setup['width']._int_,setup['height']._int_);

  if (setup['resizable'].isSet()) then
  begin
    gtk_window_set_resizable(GTK_WINDOW (el), setup['resizable']._bool_);
  end;
  if (setup['title'].isSet()) then
  begin
    gtk_window_set_title(GTK_WINDOW (el), pGchar(setup['title']._str_));
  end;
  if (setup['border-width'].isSet()) then
  begin
    gtk_container_set_border_width (GTK_CONTAINER (el), setup['border-width']._int_);
  end;
  if (setup['events']['close']['func'].isSet()) then
  begin
    if (not setup['events']['close']['data'].isSet()) then
      setup['events']['close']['data']._ptr_:=nil;
    g_signal_connect(
      G_OBJECT (el), 'delete_event',
      G_CALLBACK (setup['events']['close']['func']._ptr_),
      gpointer(setup['events']['close']['data']._ptr_)
    );
  end; 
  if (setup['events']['hide']['func'].isSet()) then
  begin
    if (not setup['events']['hide']['data'].isSet()) then
      setup['events']['hide']['data']._ptr_:=nil;
    g_signal_connect(
      G_OBJECT (el), 'window-state-event',
      G_CALLBACK (setup['events']['hide']['func']._ptr_),
      gpointer(setup['events']['hide']['data']._ptr_)
    );
  end;
  if (setup['events']['keypress']['func'].isSet()) then
  begin
    if (not setup['events']['keypress']['data'].isSet()) then
      setup['events']['keypress']['data']._ptr_:=nil;
    g_signal_connect(
      G_OBJECT (el), 'key_press_event',
      G_CALLBACK (setup['events']['keypress']['func']._ptr_),
      gpointer(setup['events']['keypress']['data']._ptr_)
    );
  end;  

  if (setup['iconify']._bool_=true) then
  begin
    gtk_window_iconify(GTK_WINDOW(el));
  end;
  if (setup['skip-taskbar-hint'].isBoolean()) then
  begin
    gtk_window_set_skip_taskbar_hint(GTK_WINDOW(el), setup['skip-taskbar-hint']._bool_);
  end;
  if (setup['decorated'].isBoolean()) then
  begin
    gtk_window_set_decorated(GTK_WINDOW(el), setup['decorated']._bool_);
  end;
  if (setup['deletable'].isBoolean()) then
  begin
    gtk_window_set_deletable(GTK_WINDOW(el), setup['deletable']._bool_);
  end;  
  gtk_widget_show(el);
  
  result:=el;
end;

function TGUIElements.TrayIcon(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
begin
  
  if fileexists(setup['file']._str_) then
  begin
    el := gtk_status_icon_new_from_file(pgchar(setup['file']._str_));
  end else begin
    exit;
    //el := gtk_status_icon_new_from_file(ico1);
  end;

  if (setup['tooltip'].isSet()) then
  begin
    gtk_status_icon_set_tooltip_text(GTK_STATUS_ICON (el), pGchar(setup['tooltip']._str_));
  end;

  if (setup['events']['click']['func'].isSet()) then
  begin
    if (not setup['events']['click']['data'].isSet()) then
      setup['events']['click']['data']._ptr_:=nil;
    g_signal_connect(
      G_OBJECT (el), 'activate',
      G_CALLBACK (setup['events']['click']['func']._ptr_),
      gpointer(setup['events']['click']['data']._ptr_)
    );
  end; 

  //gtk_widget_show (el);
  result:=el;
end;

function TGUIElements.Box(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
begin

  if (not setup['spacing'].isSet()) then setup['spacing']._int_:=3;
  if (not setup['orientation'].isSet()) then setup['orientation']._str_:='horizontal';
  
  if setup['orientation']._str_='horizontal' then
  begin
    el := gtk_hbox_new (FALSE, setup['spacing']._int_);
  end else begin
    el := gtk_vbox_new (FALSE, setup['spacing']._int_);
  end;
  if (w_parent<>nil) then
    gtk_container_add (GTK_CONTAINER (w_parent), el); 

  if (setup['width'].isInteger()) or (setup['height'].isInteger()) then
  begin
    if (not setup['width'].isInteger()) then setup['width']._int_:=-1;
    if (not setup['height'].isInteger()) then setup['height']._int_:=-1;
    gtk_widget_set_size_request(GTK_WIDGET(el), setup['width']._int_, setup['height']._int_); 
  end;

  gtk_widget_show (el);
  result:=el;
end;


function TGUIElements.Caption(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
begin
  if (not setup['label'].isSet()) then setup['label']._str_:='...';
  el:=gtk_label_new(pgchar(setup['label']._str_));
  if (setup['events']['clicked']['func'].isSet()) then
  begin
    if (not setup['events']['clicked']['data'].isSet()) then
      setup['events']['clicked']['data']._ptr_:=nil;
    g_signal_connect(G_OBJECT (el),
      'clicked',
      G_CALLBACK (setup['events']['clicked']['func']._ptr_),
      setup['events']['clicked']['data']._ptr_
    );
  end;

  if (not setup['spacing'].isSet()) then
    setup['spacing']._int_:=3;
  
  gtk_box_pack_start(GTK_BOX (w_parent), el, TRUE, TRUE, setup['spacing']._int_);
  gtk_widget_show (el);
  result:=el;
end;

function TGUIElements.Input(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
begin
  el:=gtk_entry_new();
  if (setup['events']['changed']['func'].isSet()) then
  begin
    if (not setup['events']['changed']['data'].isSet()) then
      setup['events']['changed']['data']._ptr_:=nil;
    g_signal_connect(G_OBJECT (el),
      'changed',
      G_CALLBACK (setup['events']['changed']['func']._ptr_),
      gpointer (setup['events']['changed']['data']._ptr_)
    );
  end;

  if (setup['text'].isString()) then
  begin
    setValue(setup, setup['text']);  
    //gtk_entry_set_text(Gtk_Entry(el), pgchar(setup['text']._str_));
  end;
  
  if (not setup['spacing'].isSet()) then
    setup['spacing']._int_:=3;
  
  gtk_box_pack_start(GTK_BOX (w_parent), el, TRUE, TRUE, setup['spacing']._int_);
  gtk_widget_show (el);
  result:=el;
end;

function TGUIElements.Button(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
var p:pointer;
var pixbuf: pGDKPixbuf;
var err:pGerror;
var xSize,ySize:integer;
begin
  if (not setup['label'].isSet()) then setup['label']._str_:='...';
  el:=gtk_button_new_with_mnemonic(pgchar(setup['label']._str_));
  if (setup['events']['click']['func'].isSet()) then
  begin
    if (not setup['events']['click']['data'].isSet()) then
      setup['events']['click']['data']._ptr_:=nil;
    g_signal_connect(G_OBJECT (el),
      'clicked',
      G_CALLBACK (setup['events']['click']['func']._ptr_),
      setup['events']['click']['data']._ptr_
    );
    //writeln(pchar(setup['events']['click']['data']._ptr_));
  end;

  if (setup['image']['filename'].isString()) then
  if fileexists(setup['image']['filename']._str_) then
  begin
    xSize:=30;ySize:=30;
    if (setup['image']['width'].isInteger()) then xSize:=setup['image']['width']._int_;
    if (setup['image']['height'].isInteger()) then xSize:=setup['image']['height']._int_;
    //writeln(setup['image']['filename']._str_);
    pixbuf := gdk_pixbuf_new_from_file_at_size(pgchar(setup['image']['filename']._str_),xSize,ySize, @err);
    //pixbuf := gdk_pixbuf_scale_simple(pixbuf, 36, 36, GDK_INTERP_BILINEAR);
    //setup['image']['el']._ptr_:=gtk_image_new_from_file(pgchar(setup['image']['filename']._str_));
    setup['image']['el']._ptr_:=gtk_image_new_from_pixbuf(pixbuf);
    //gtk_button_set_always_show_image(GTK_BUTTON(el), true);
    gtk_button_set_image(GTK_BUTTON(el), GTK_WIDGET(setup['image']['el']._ptr_));
    if (setup['image']['only'].isBoolean()) then
      if setup['image']['only']._bool_ = true then
        begin
          gtk_button_set_label(GTK_BUTTON(el), nil);
        end;
  end;

  if (setup['width'].isInteger()) or (setup['height'].isInteger()) then
  begin
    if (not setup['width'].isInteger()) then setup['width']._int_:=-1;
    if (not setup['height'].isInteger()) then setup['height']._int_:=-1;
    gtk_widget_set_size_request(GTK_WIDGET(el), setup['width']._int_, setup['height']._int_); 
  end;
  
  if (not setup['spacing'].isSet()) then
    setup['spacing']._int_:=3;
    
  gtk_box_pack_start(GTK_BOX (w_parent), el, TRUE, TRUE, setup['spacing']._int_);
  gtk_widget_show (el);
  result:=el;
end;


function TGUIElements.ToggleButton(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
begin
  if (not setup['label'].isSet()) then setup['label']._str_:='...';
  if (not setup['active'].isSet()) then setup['active']._bool_:=false;
  el:=gtk_toggle_button_new_with_label(pgchar(setup['label']._str_));
  if (setup['events']['click']['func'].isSet()) then
  begin
    if (not setup['events']['click']['data'].isSet()) then
      setup['events']['click']['data']._ptr_:=nil;
    g_signal_connect(G_OBJECT (el),
      'clicked',
      G_CALLBACK (setup['events']['click']['func']._ptr_),
      gpointer (setup['events']['click']['data']._ptr_)
    );
  end;
  
  gtk_toggle_button_set_active(Gtk_Toggle_Button(el), gboolean(setup['active']._bool_));

  if (not setup['spacing'].isSet()) then
    setup['spacing']._int_:=3;
  
  gtk_box_pack_start(GTK_BOX (w_parent), el, TRUE, TRUE, setup['spacing']._int_);
  gtk_widget_show (el);
  result:=el;
end;


function TGUIElements.CheckButton(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
begin
  if (not setup['label'].isSet()) then setup['label']._str_:='...';
  if (not setup['active'].isSet()) then setup['active']._bool_:=false;

  el:=gtk_check_button_new_with_label(pgchar(setup['label']._str_));
  if (setup['events']['click']['func'].isSet()) then
  begin
    if (not setup['events']['click']['data'].isSet()) then
      setup['events']['click']['data']._ptr_:=nil;
    g_signal_connect(G_OBJECT (el),
      'clicked',
      G_CALLBACK (setup['events']['click']['func']._ptr_),
      gpointer (setup['events']['click']['data']._ptr_)
    );
  end;

  gtk_toggle_button_set_active(Gtk_Toggle_Button(el), gboolean(setup['active']._bool_));

  if (not setup['spacing'].isSet()) then
    setup['spacing']._int_:=3;
    
  
  gtk_box_pack_start(GTK_BOX (w_parent), el, TRUE, TRUE, setup['spacing']._int_);
  gtk_widget_show (el);
  result:=el;
end;


function TGUIElements.RadioButton(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
begin
  if (not setup['label'].isSet()) then setup['label']._str_:='...';
  if (not setup['group'].isSet()) then setup['group']._ptr_:=nil;
  if (not setup['active'].isSet()) then setup['active']._bool_:=false;
  
  if (setup['buddy'].isSet()) then
  begin
    el:=gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(setup['buddy']._ptr_), pgchar(setup['label']._str_));
  end else begin
    el:=gtk_radio_button_new_with_label(setup['group']._ptr_, pgchar(setup['label']._str_));
  end;
  
  setup['group']._ptr_:=gtk_radio_button_get_group(GTK_RADIO_BUTTON(el));
  if (setup['events']['click']['func'].isSet()) then
  begin
    if (not setup['events']['click']['data'].isSet()) then
      setup['events']['click']['data']._ptr_:=nil;
    g_signal_connect(G_OBJECT (el),
      'clicked',
      G_CALLBACK (setup['events']['click']['func']._ptr_),
      gpointer (setup['events']['click']['data']._ptr_)
    );
  end;
  
  gtk_toggle_button_set_active(Gtk_Toggle_Button(el), gboolean(setup['active']._bool_));

  if (not setup['spacing'].isSet()) then
    setup['spacing']._int_:=3;
  
  gtk_box_pack_start(GTK_BOX (w_parent), el, TRUE, TRUE, setup['spacing']._int_);
  gtk_widget_show (el);
  result:=el;
end;


function TGUIElements.ProgressBar(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
begin
  //if (setup['label'].isSet()) then setup['label']._str_:='...';
  
  el:=gtk_progress_bar_new();
  
  if (setup['label'].isSet()) then
    gtk_progress_bar_set_text(GTK_PROGRESS_BAR(el), pgchar(setup['label']._str_));

  if (not setup['spacing'].isSet()) then
    setup['spacing']._int_:=3;
    
  //gtk_progress_bar_pulse(GTK_PROGRESS_BAR(el));
  
  gtk_box_pack_start(GTK_BOX (w_parent), el, TRUE, TRUE, setup['spacing']._int_);
  gtk_widget_show (el);
  result:=el;
end;


function TGUIElements.RadioButtonGroup(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:PGtkWidget;
begin
  //setup['el']._ptr_:=g_slist_alloc();
  result:=w_parent;
end;


var iter:pgtkTreeIter;

function TGUIElements.SetTableRows(setup, items:TTypeCast):boolean;
var list_store:pGtkListStore;
var row1:TTypeCast=nil;
var row2:TTypeCast=nil;
var i, j:integer;
var a:PGChar;
var gval:tGValue;
var el:PGtkWidget;
begin
  el:=setup['el']._ptr_;
  setup['rows'].copy(items);
  //gval:=G_VALUE_INIT;
  fillbyte(gval, sizeof(tGValue),0);
  //exit;
  list_store:=gtk_tree_view_get_model(GTK_TREE_VIEW(el));
  if not GTK_IS_LIST_STORE(list_store) then exit;
  
  gtk_widget_hide(GTK_WIDGET(el));
  gtk_list_store_clear(list_store);
  //selection := gtk_tree_view_get_selection(GTK_TREE_VIEW(list1));
  items.reset(row1);
  //getmem(iter, sizeof(pgtkTreeIter)+1);
  i:=0;
  while items.each(row1) do
  begin
    g_value_init(@gval, G_TYPE_INT);
    //writeln(': init');
    gtk_list_store_append(GTK_LIST_STORE(list_store), @iter);
    //gtk_list_store_set(GTK_LIST_STORE(list_store), @iter, [0, pgchar(''),1, pgchar(''),2, pgchar(''),3, pgchar(''),4, pgchar(''),5, pgchar(''),6, pgchar(''),7, pgchar(''),8, pgchar(''),9, pgchar(''),10, pgchar(''),11, pgchar(''),12, pgchar(''),13, pgchar(''),14, pgchar(''),15, pgchar(''),16, pgchar(''),17, pgchar(''),18, pgchar(''),19, pgchar(''), -1]);
    row1.reset(row2);
    j:=0;
    while row1.each(row2) do
    begin
      if J>0 then
      begin
        //writeln(row2._str_);
        a:= strpg(a, row2._str_);
        gtk_list_store_set(GTK_LIST_STORE(list_store), @iter, j, pgchar(a), -1);
      end;
      inc(j);
    end;
    
    
    g_value_set_int(@gval, i);
    //writeln(':set int');
    gtk_list_store_set_value(
      GTK_LIST_STORE(list_store),
      @iter,
      0,
      @gval  
    );
    //writeln(':store');
    
    inc(i);
    g_value_unset(@gval);
    //writeln(':unset');
  end;
  
  //freemem(iter, sizeof(pgtkTreeIter)+1);
  row1.free();
  row2.free();
  gtk_widget_show(GTK_WIDGET(el));
end;

function TGUIElements.Table(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var row:TTypeCast=nil;
//var scrolled_window:PGtkWidget;
var el:PGtkWidget;
var list_store:pGtkListStore;
var cell       : PGtkCellRenderer;
var selection  : PGtkTreeSelection;
var column: PGTKTreeViewColumn;
var adj: PGTKObject;
var L:array[0..20]of GType;
//
//var a,b,c,d:string;
//var pglabel:PGChar;
var i:byte;
begin

  if (not setup['spacing'].isInteger()) then setup['spacing']._int_:=3;
  if (not setup['selection-mode'].isInteger()) then setup['selection-mode']._int_:=GTK_SELECTION_SINGLE;
  
  
  setup['columns'].reset(row);
  for i:=0 to 20 do L[i]:=G_TYPE_STRING;
  i:=0;
  while setup['columns'].each(row) do
  begin
    if not row['type'].isString() then row['type']._str_:='string';
    case row['type']._str_ of
      'string':begin
          L[i]:=G_TYPE_STRING;
        end;
      'pixbuf':begin
          L[i]:=GDK_TYPE_PIXBUF;
        end;
      'index':begin
          L[i]:=G_TYPE_INT;
        end;
      'integer':begin
          L[i]:=G_TYPE_INT;
        end;
    end;
    inc(i);
  end;

  list_store:=gtk_list_store_newv(20, @L);
  //list_store:=gtk_list_store_new(1, [G_TYPE_STRING, -1]);

  setup['scrolls']['el']._ptr_ := gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(
    GTK_SCROLLED_WINDOW(setup['scrolls']['el']._ptr_),
    GTK_POLICY_AUTOMATIC,
    GTK_POLICY_AUTOMATIC
  );
  gtk_scrolled_window_set_shadow_type(
    GTK_SCROLLED_WINDOW(setup['scrolls']['el']._ptr_),
    GTK_SHADOW_IN
  );
  adj:=gtk_adjustment_new (100,50,200,1,10,20);
  
  el := gtk_tree_view_new ();
  gtk_tree_view_set_model(
    GTK_TREE_VIEW (el),
    GTK_TREE_MODEL (list_store)
  );


  g_object_unref(list_store);
  
  gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(el), true);
  gtk_tree_view_set_vadjustment(GTK_TREE_VIEW(el), GTK_ADJUSTMENT(adj));
  gtk_tree_view_set_hadjustment(GTK_TREE_VIEW(el), GTK_ADJUSTMENT(adj));    
  
  selection := gtk_tree_view_get_selection(GTK_TREE_VIEW(el));
  //gtk_tree_selection_set_mode (GTK_TREE_SELECTION (selection),GTK_SELECTION_MULTIPLE);//GTK_SELECTION_BROWSE
  
  gtk_tree_selection_set_mode(
    GTK_TREE_SELECTION(selection),
    setup['selection-mode']._int_
  );
  
  gtk_widget_set_size_request(el, 200, -1);
  //cell := gtk_cell_renderer_text_new ();
  //g_object_set (G_OBJECT (cell),'style', [ PANGO_STYLE_ITALIC, NULL ]);
  //****************REPEAT
  setup['columns'].reset(row);
  i:=0;
  while setup['columns'].each(row) do
  begin
    if not row['label'].isString() then row['label']._str_:='...';
    if not row['type'].isString() then row['type']._str_:='string';
    if not row['width'].isInteger() then row['width']._int_:=100;
    case row['type']._str_ of
      'pixbuf':begin
        cell:=gtk_cell_renderer_pixbuf_new();
        //g_object_set (G_OBJECT (cell),'style', [ PANGO_STYLE_ITALIC, NULL ]);
        column:=gtk_tree_view_column_new_with_attributes(
          pgchar(row['label']._str_),
          cell,
          [ 
            pgchar('pixbuf'),
            i,
            nil
          ]
        );//[pgchar('text'),i,null][null]
        gtk_tree_view_column_set_min_width(
          GTK_TREE_VIEW_COLUMN(column),
          gint(row['width']._int_)
        );
        gtk_tree_view_append_column(
          GTK_TREE_VIEW(el),
          column
        );   
              
        end;
      'string':begin
        cell:=gtk_cell_renderer_text_new();
        g_object_set(
          G_OBJECT(cell),
          'style',
          [ PANGO_STYLE_ITALIC, NULL ]
        );
        column:=gtk_tree_view_column_new_with_attributes(
          pgchar(row['label']._str_),
          cell,
          [
            pgchar('text'),
            i,
            nil
          ]
        );//[pgchar('text'),i,null][null]
        gtk_tree_view_column_set_min_width(
          GTK_TREE_VIEW_COLUMN(column),
          gint(row['width']._int_)
        );
        gtk_tree_view_append_column(
          GTK_TREE_VIEW(el),
          column
        );   
           
        end;
      'index':begin
        cell:=gtk_cell_renderer_text_new();
        gtk_cell_renderer_set_visible(Gtk_Cell_Renderer(cell),false);
        end;
    end;
    inc(i);  
  end;

  gtk_container_add(GTK_CONTAINER(setup['scrolls']['el']._ptr_), el);
  gtk_box_pack_start(GTK_BOX(w_parent), setup['scrolls']['el']._ptr_, TRUE, TRUE, setup['spacing']._int_);

  //g_signal_connect (G_OBJECT(list1), 'row-activated', G_CALLBACK(cb_callback), nil);
  //g_signal_connect (G_OBJECT(list1), 'button-release-event', G_CALLBACK(cb_callback), nil);
  //g_object_set(el, pgchar('activate-on-single-click'),true);

  if (setup['events']['click']['func'].isSet()) then
  begin
    if (not setup['events']['click']['data'].isSet()) then
      setup['events']['click']['data']._ptr_:=nil;
    g_signal_connect(G_OBJECT (el),
      'cursor-changed',
      G_CALLBACK (setup['events']['click']['func']._ptr_),
      gpointer (setup['events']['click']['data']._ptr_)
    );//'button-release-event'
  end;

  if (setup['events']['click']['func'].isSet()) then
  begin
    if (not setup['events']['click']['data'].isSet()) then
      setup['events']['click']['data']._ptr_:=nil;
    g_signal_connect(G_OBJECT (el),
      'button-release-event',
      G_CALLBACK (setup['events']['click']['func']._ptr_),
      gpointer (setup['events']['click']['data']._ptr_)
    );//'button-release-event'
  end;

  gtk_widget_show(setup['scrolls']['el']._ptr_);
  gtk_widget_show(el);
  //g_object_unref(scrolled_window);
  result:=el;

end;

function TGUIElements.Text(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var adj: PGTKObject;
var el:PGtkWidget;
begin
  //setup['buffer']['el']._ptr_:=gtk_text_buffer_new(nil);
  setup['scrolls']['el']._ptr_:= gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(
    GTK_SCROLLED_WINDOW(setup['scrolls']['el']._ptr_ ),
    GTK_POLICY_AUTOMATIC,
    GTK_POLICY_AUTOMATIC
  );
  gtk_scrolled_window_set_shadow_type(
    GTK_SCROLLED_WINDOW(setup['scrolls']['el']._ptr_ ),
    GTK_SHADOW_IN
  );
  //adj:=gtk_adjustment_new (100,50,200,1,10,20);
  //el:=gtk_text_view_new_with_buffer(setup['buffer']['el']._ptr_);
  el:=gtk_text_view_new();
  gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(el), GTK_WRAP_WORD);
  //setup['buffer']['el']._ptr_:=gtk_text_view_get_buffer(Gtk_Text_View(el));
  
    //gtk_text_view_set_vadjustment (GTK_TEXT_VIEW (text1),GTK_ADJUSTMENT(adj));
  gtk_widget_set_size_request(el, 200, -1);

  gtk_container_add(GTK_CONTAINER(setup['scrolls']['el']._ptr_ ), el);
  gtk_box_pack_start(GTK_BOX(w_parent), setup['scrolls']['el']._ptr_ , TRUE, TRUE, 3);
  gtk_widget_show (el);
  gtk_widget_show(setup['scrolls']['el']._ptr_);
  result:=el;
end;

function TGUIElements.Image(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var adj: PGTKObject;
var el:PGtkWidget;
begin
  exit;
  setup['loader']['el']._ptr_:=gdk_pixbuf_loader_new();
  setup['pixbuf']['el']._ptr_:=gtk_text_buffer_new(nil);
  setup['scrolls']['el']._ptr_:= gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(
    GTK_SCROLLED_WINDOW(setup['scrolls']['el']._ptr_ ),
    GTK_POLICY_AUTOMATIC,
    GTK_POLICY_AUTOMATIC
  );
  gtk_scrolled_window_set_shadow_type(
    GTK_SCROLLED_WINDOW(setup['scrolls']['el']._ptr_ ),
    GTK_SHADOW_IN
  );
  //adj:=gtk_adjustment_new (100,50,200,1,10,20);
  //el:=gtk_text_view_new_with_buffer(setup['buffer']['el']._ptr_);
    //gtk_text_view_set_vadjustment (GTK_TEXT_VIEW (text1),GTK_ADJUSTMENT(adj));
  //gtk_widget_set_size_request(el, 200, -1);

  el := gtk_image_new_from_pixbuf(GDK_PIXBUF(setup['pixbuf']['el']._ptr_));
  gtk_scrolled_window_add_with_viewport(
    GTK_SCROLLED_WINDOW(setup['scrolls']['el']._ptr_),
    el
  );
 

  gtk_container_add(GTK_CONTAINER(setup['scrolls']['el']._ptr_ ), el);
  gtk_box_pack_start(GTK_BOX(w_parent), setup['scrolls']['el']._ptr_ , TRUE, TRUE, 3);
  gtk_widget_show (el);
  gtk_widget_show(setup['scrolls']['el']._ptr_);
  result:=el;
end;

function TGUIElements.SetSelectRows(setup, items:TTypeCast):boolean;
var list_store:pGtkListStore;
var row1:TTypeCast=nil;
var row2:TTypeCast=nil;
var i, j:integer;
var a:PGChar;
var el:PGtkWidget;
begin
  el:=setup['el']._ptr_;
  //exit;
  list_store:=gtk_combo_box_get_model(GTK_COMBO_BOX(el));
  if not GTK_IS_LIST_STORE(list_store) then exit;
  
  gtk_widget_hide(GTK_WIDGET(el));
  gtk_list_store_clear(list_store);
  //selection := gtk_tree_view_get_selection(GTK_TREE_VIEW(list1));
  items.reset(row1);
  //getmem(iter, sizeof(pgtkTreeIter)+1);
  while items.each(row1) do
  begin
    gtk_list_store_append(GTK_LIST_STORE(list_store), @iter);
    gtk_list_store_set(GTK_LIST_STORE(list_store), @iter, 0, pgchar(row1._str_), -1);
    {
    row1.reset(row2);
    j:=0;
    while row1.each(row2) do
    begin
      a:= strpg(a, row2._str_);
      gtk_list_store_set(GTK_LIST_STORE(list_store), @iter, j, pgchar(a), -1);
      inc(j);
    end;
    }
  end;
  //freemem(iter, sizeof(pgtkTreeIter)+1);
  row1.free();
  //row2.free();
  gtk_widget_show(GTK_WIDGET(el));
end;


function TGUIElements.Select(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var iter: pgtkTreeIter;
var column:pGtkCellRenderer;
var list_store:pGtkListStore;
var row:TTypeCast=nil;                   
var el:PGtkWidget;
begin
  if (not setup['spacing'].isSet()) then
    setup['spacing']._int_:=3;

  if (not setup['index'].isSet()) then
    setup['index']._int_:=0;

  list_store:=gtk_list_store_new(1, [G_TYPE_STRING, -1]);

  el:=gtk_combo_box_new_with_model(GTK_LIST_STORE(list_store));
  
  gtk_box_pack_start(
    GTK_BOX (w_parent),
    el,
    TRUE, TRUE,
    setup['spacing']._int_
  );
  gtk_widget_show (el);
  
  if (setup['events']['changed']['func'].isSet()) then
  begin
    if (not setup['events']['changed']['data'].isSet()) then
      setup['events']['changed']['data']._ptr_:=nil;

    g_signal_connect(
      G_OBJECT(el),
      'changed',
      G_CALLBACK(setup['events']['changed']['func']._ptr_),
      setup['events']['changed']['data']._ptr_
    );
  end;
  
  setup['el']._ptr_:=el;

  if (setup['items'].isArray()) then
  begin
    SetSelectRows(setup,setup['items']);
    {
    gtk_list_store_clear(GTK_LIST_STORE(list_store));
    setup['items'].reset(row);
    while setup['items'].each(row) do
    begin
      gtk_list_store_insert_with_values(
        GTK_LIST_STORE(list_store),
        @iter,
        -1,
        0,
        pgchar(row._str_),
        -1
      );
    
    end;
    row.free();
    }
  end;
  
  g_object_unref(list_store);
  
  column := gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(el), column, TRUE);

  gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(el), column,
                                   //'cell-background', 0,
                                   'text', 0,
                                   Nil);
  
  //gtk_combo_box_set_id_column (el, 1);
  
  gtk_combo_box_set_active(Gtk_Combo_Box(el), setup['index']._int_);
   
                                   
  result:=el;

end;

function TGUIElements.Notebook(w_parent:PGtkWidget;setup:TTypeCast):PGtkWidget;
var el:pGTKWidget;
begin
  if (not setup['spacing'].isSet()) then
    setup['spacing']._int_:=3;

  el:=gtk_notebook_new();
  
  gtk_box_pack_start(
    GTK_BOX (w_parent),
    el,
    TRUE, TRUE,
    setup['spacing']._int_
  );
  
  gtk_widget_show (el);

  result:=el;
end;

function TGUIElements.NotebookAppendPages(setup, items:TTypeCast):boolean;
var el,capt:PGtkWidget;
var ROW:TTypeCast=nil;
begin
  el:=setup['el']._ptr_;
  items.reset(ROW);
  while items.each(ROW) do
  begin
    capt:=gtk_label_new_with_mnemonic(pgchar(ROW['label']._str_));
    gtk_notebook_append_page(GTK_NOTEBOOK(el), GTK_WIDGET(ROW['el']._ptr_), GTK_WIDGET(capt));
  end;
end;

function TGUIElements.SetValue(setup, items:TTypeCast):boolean;
var buffer:pGTKTextBuffer;
var start_iter:tGTKTextIter;
var path:pGTKTreePath;
var selection:pGTKTreeSelection;
var column:pGtkTreeViewColumn;
var el:PGtkWidget;
var ROW:TTypeCast=nil;
begin
  el:=setup['el']._ptr_;
  if (GTK_IS_TREE_VIEW(el)) then
  begin
    if items.isBoolean() then
    begin
      selection:=gtk_tree_view_get_selection(GTK_TREE_VIEW(el));
      if items._bool_ = true then
        gtk_tree_selection_select_all(selection);
      if items._bool_ = false then
        gtk_tree_selection_unselect_all(selection);
    end;
    if items.isString() then
    begin
      selection:=gtk_tree_view_get_selection(GTK_TREE_VIEW(el));
      //gtk_tree_selection_select_all(selection);
      path:=gtk_tree_path_new_from_string(pchar(items._str_));
      //writeln(gtk_tree_path_to_string(path));
      gtk_tree_selection_select_path(selection, path);
      gtk_tree_path_free(path);
    end;
    if items.isInteger() then
    begin
      selection:=gtk_tree_view_get_selection(GTK_TREE_VIEW(el));
      //gtk_tree_selection_select_all(selection);
      path:=gtk_tree_path_new_from_indices(items._int_, -1);
      //writeln(gtk_tree_path_to_string(path));
      gtk_tree_selection_select_path(selection, path);
      //column:=gtk_tree_view_get_column(GTK_TREE_VIEW(el),1);
      //gtk_tree_view_row_activated(GTK_TREE_VIEW(el), path, column);
      gtk_tree_path_free(path);
    end;
    if items.isArray() then
    begin
      items.reset(ROW);
      while items.each(ROW) do
      begin
        SetValue(setup, ROW);
      end;
      ROW.free();ROW:=nil;
    end;    
  end;

  if (GTK_IS_TEXT_VIEW(el)) then
  begin
    //buffer:=setup['buffer']['el']._ptr_;
    buffer:=gtk_text_view_get_buffer(Gtk_Text_View(el));
    
    gtk_text_buffer_set_text(Gtk_Text_Buffer(buffer),
      pgchar(items._str_),
      -1
    );
    {
    gtk_text_buffer_get_iter_at_offset(buffer, @start_iter, -1);
    
    gtk_text_buffer_insert(
      Gtk_Text_Buffer(buffer),
      @start_iter,
      pgchar(items._str_),
      -1
    );
    }
  end;
  if (GTK_IS_COMBO_BOX(el)) then
  begin
    gtk_combo_box_set_active(Gtk_Combo_Box(el), items._int_);
  end;

  if (GTK_IS_ENTRY(el)) then
  begin
    gtk_entry_set_text(Gtk_Entry(el), pgchar(items._str_));
  end;
  if (GTK_IS_LABEL(el)) then
  begin
    gtk_label_set_text(Gtk_LABEL(el), pgchar(items._str_));
  end; 
  if (GTK_IS_PROGRESS_BAR(el)) then
  begin
    if (items.isString()) then
      gtk_progress_bar_set_text(Gtk_Progress_Bar(el), pgchar(items._str_));
    if (items.isInteger()) then
      gtk_progress_bar_set_fraction(Gtk_Progress_Bar(el), (items._int_/100));
    if (items.isBoolean()) and (items._bool_=true) then
    begin
      //gtk_progress_bar_pulse(GTK_PROGRESS_BAR(el));
    end;
    if (items.isBoolean()) and (items._bool_=false) then
    begin
      //gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(el), 0);
    end;
  end;

  if (GTK_IS_BUTTON(el)) then
  begin
    if (items.isString()) then
      gtk_button_set_label(Gtk_button(el), pgchar(items._str_));
  end;

  if (GTK_IS_TOGGLE_BUTTON(el)) then
  begin
    if (items.isString()) then
      gtk_button_set_label(Gtk_button(el), pgchar(items._str_));
    if (items.isBoolean()) then
      gtk_toggle_button_set_active(Gtk_Toggle_button(el), gboolean(items._bool_));
  end;

  if (GTK_IS_CHECK_BUTTON(el)) then
  begin
    if (items.isString()) then
      gtk_button_set_label(Gtk_button(el), pgchar(items._str_));
    if (items.isBoolean()) then
      gtk_toggle_button_set_active(Gtk_toggle_button(el), gboolean(items._bool_));
  end;


  if (GTK_IS_WINDOW(el)) then
  begin
    gtk_window_set_title(Gtk_Window(el), pgchar(items._str_));
  end;

  if (GTK_IS_STATUS_ICON(el)) then
  begin
    if (fileexists(items._str_)) then
    begin
      gtk_status_icon_set_from_file(GTK_STATUS_ICON(el), pgchar(items._str_));
    end else begin
      gtk_status_icon_set_tooltip_text(GTK_STATUS_ICON(el), pGchar(items._str_));
    end;
  end;
  
end;


  procedure view_selected_foreach_func(model:pGtkTreeModel;
                              path:pGtkTreePath;
                              iter:pGtkTreeIter;
                              userdata:gpointer); cdecl;
    var gval:tGValue;
    var i:integer;
    var TC:TTypeCast;
  begin
    TC:=TTypeCast.Create();
    TC._str_:=gtk_tree_path_to_string(path);
    TTypeCast(userdata).array_push(TC);
    TC.free();
    exit;
    {
    fillbyte(gval, sizeof(tGValue),0);
    
    gtk_tree_model_get_value(model, iter, 0, @gval);
    TC._int_:=g_value_get_int(@gval);
    TTypeCast(userdata).array_push(TC);
    TC.free();
    g_value_unset(@gval);
    }
  end;


function TGUIElements.GetValue(setup:TTypeCast):TTypeCast;
var buffer:pGTKTextBuffer;
var el:PGtkWidget;
var p:pgchar;
var start_sel:tGtkTextIter;
var end_sel:tGtkTextIter;
var selection: pGtkTreeSelection; 
var TT:TTypeCast;
begin
  TT:=TTypeCast.create();
  el:=setup['el']._ptr_;
  if (GTK_IS_TEXT_VIEW(el)) then
  begin
    //buffer:=setup['buffer']['el']._ptr_;
    buffer:=gtk_text_view_get_buffer(Gtk_Text_View(el));
    if (not GTK_IS_TEXT_BUFFER(buffer)) then writeln('not buffer!');
    
    gtk_text_buffer_get_bounds(
      Gtk_Text_Buffer(buffer),
      @start_sel,
      @end_sel
    );

    p:=gtk_text_buffer_get_text(Gtk_Text_Buffer(buffer),
      @start_sel,
      @end_sel,
      false
    );
    //writeln(p);
    TT._str_:=strpas(p);
  end;

  if (GTK_IS_TREE_VIEW(el)) then
  begin

    TT.clear();
    selection:=gtk_tree_view_get_selection(GTK_TREE_VIEW(el));
    
    gtk_tree_selection_selected_foreach(
      selection,
      @view_selected_foreach_func,
      TT
    );

    //TT._int_:=gtk_combo_box_get_active(Gtk_Combo_Box(el));
  end;

  if (GTK_IS_COMBO_BOX(el)) then
  begin
    TT._int_:=gtk_combo_box_get_active(Gtk_Combo_Box(el));
  end;

  if (GTK_IS_ENTRY(el)) then
  begin
    TT._str_:=strpas(gtk_entry_get_text(Gtk_Entry(el)));
  end;
  if (GTK_IS_LABEL(el)) then
  begin
    //gtk_label_set_text(Gtk_LABEL(el), pgchar(items._str_));
  end; 
  if (GTK_IS_PROGRESS_BAR(el)) then
  begin

  end;

  if (GTK_IS_BUTTON(el)) then
  begin

  end;

  if (GTK_IS_TOGGLE_BUTTON(el)) then
  begin
    TT._bool_:=gtk_toggle_button_get_active(Gtk_Toggle_button(el));
  end;

  if (GTK_IS_CHECK_BUTTON(el)) then
  begin
    TT._bool_:=gtk_toggle_button_get_active(Gtk_toggle_button(el));
  end;

  if (GTK_IS_WINDOW(el)) then
  begin
    //gtk_window_set_title(Gtk_Window(el), pgchar(items._str_));
  end;
  
  result:=TT;
end;

procedure TGUIElements.sensing(setup:TTypeCast; b:boolean);
var el:pGTKWidget;
begin
  el:=setup['el']._ptr_;
  setup['sensitive']._bool_:=b;
  if setup['sensitive'].isBoolean() then
    gtk_widget_set_sensitive(GTK_WIDGET(el), setup['sensitive']._bool_);
end;

procedure TGUIElements.visible(setup:TTypeCast);
var el:pGTKWidget;
begin
  el:=setup['el']._ptr_;
  if not setup['visible'].isBoolean() then exit;
  if setup['visible']._bool_ then
  begin
    gtk_widget_show(GTK_WIDGET(el));
  end else begin
    gtk_widget_hide(GTK_WIDGET(el));
  end;
end;

begin

end.
