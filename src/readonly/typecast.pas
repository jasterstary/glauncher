unit Typecast;

{$mode objfpc}
{$LONGSTRINGS ON}

interface

uses
  Classes, SysUtils, Strings, cwstring, Variants, fpjson, jsonparser, Math;


type
pTypecastCallback = ^fTypecastCallback;
fTypecastCallback = procedure(tc: pointer);

Float = Single;

TTypecastValue = record
    ptr: pointer;
    size: longint;
    cast: byte;
    mode: byte;
end;


TIter = record
  idx: integer;
  key: variant;
  value: pointer;
end;

TTypecast = class
  private
    val: TTypecastValue;
    idx: Longint;
    iter: Longint;
    key: variant;
    fReadCallback:pTypecastCallback;
    fWriteCallback:pTypecastCallback;
    iSetup: integer;
    owner: TTypecast;
    confirmed:boolean;
    
    
    Values: array of TTypecast;
    procedure _dump(val1:TTypecast; indent:string);
    function _JSONA(val1:TTypecast):TJSONArray;
    function _JSONO(val1:TTypecast):TJSONObject;
    function _JSONI(val1:TTypecast;jData : TJSONData):boolean;
    procedure WriteCallback();
    
  protected
    procedure setOwner(o:TTypecast);

    
  public
  
    
    constructor Create();
    destructor Destroy();override;
    
    procedure clear();
    
    
    procedure setSetup(i: integer);
    function getSetup():integer;
    //procedure setReadCallback(f: pTypecastCallback);
    //procedure setWriteCallback(f: pTypecastCallback);
    //procedure setWriteCallback(f: pointer);

    procedure _cast(Value: integer);
    function cast_(): integer;    
    property _cast_:integer read cast_ write _cast;

    procedure _type(Value: string);
    function type_(): string;    
    property _type_:string read type_ write _type;
    
    procedure _key(Value: Variant);
    function key_(): Variant;
    function keystr_(): string;    
    property _key_:Variant read key_ write _key;
    property _keystr_:string read keystr_;

    function owner_(): Variant;    
    property _owner_:Variant read owner_;

    procedure _idx(Value: Longint);
    function idx_(): Longint;    
    property _idx_:Longint read idx_ write _idx;    
  
    procedure _bool(Value: Boolean);
    function bool_(): Boolean;    
    property _bool_:Boolean read bool_ write _bool;
    
    procedure _int(Value: Integer);
    function int_(): Integer;    
    property _int_:Integer read int_ write _int;

    procedure _float(Value: Float);
    function float_(): Float;    
    property _float_:Float read float_ write _float;
    
    procedure _str(Value: AnsiString);
    function str_(): AnsiString;    
    property _str_:AnsiString read str_ write _str;


    function var_(): variant;    
    property _var_:variant read var_;
    
    //property this: Integer read int_; default;
    
    procedure _pchar(Value: Pchar);
    function pchar_(): Pchar;    
    property _pchar_:Pchar read pchar_ write _pchar;

    procedure _ptr(Value: Pointer);
    function ptr_(): Pointer;    
    property _ptr_:Pointer read ptr_ write _ptr;
{
    procedure _array(Value: Pointer);
    function array_(): Pointer;    
    property _array_:Pointer read array_ write _array;
}
    procedure _fld (Index: Variant; Value: TTypecast);
    function fld_ (Index: Variant): TTypecast;
    property _fld_[Index: Variant]: TTypecast read fld_; default;
    
    procedure _json(s: ansistring);
    function json_(): ansistring;
    property _json_: ansistring read json_ write _json;
    
    function size_(): longint;
    property _size_: longint read size_;

    function _Index_ (Index: Variant): Integer;
    function Count(): Integer;

    procedure reset(var row: TTypeCast);
    function Each(var row: TTypeCast): Boolean;


    function Foreach(var row: TIter): Boolean;
    procedure ForeachStart(var row: TIter);
    procedure ForeachEnd(var row: TIter);
    function Select(Index: Variant;var row: TIter): Boolean;

    function isSet():boolean;
    function isEmpty():boolean;
    function isIndexedArray():boolean;
    function isArray():boolean;
    function isString():boolean;
    function isBoolean():boolean;
    function isInteger():boolean;
    function isReal():boolean;
    function isPchar():boolean;
    function isPointer():boolean;
    function isBlob():boolean;
    function isChars():boolean;
    function isZeroTerminated():boolean;

    function zeroTerminate():boolean;
    function convertToString():boolean;
    function expandJSON():boolean;
    function blockwrite(buffer:pointer;size:longint;blocksize:integer):boolean;
    function blockread(buffer:pointer;size:longint;blocksize:integer):boolean;
    function explode(delimiter:string):boolean;
    
    function printhex():boolean;
    function print():boolean;
    procedure dump();
    function _dumpType(Index: Variant): Integer;        
    function toJSON():ansistring;
    function fromJSON(json:ansistring): boolean;
    function fromJSONpchar(json:pchar): boolean;
    function array_keys():TTypecast;
    function array_values():TTypecast;
    function array_shift(var TT:TTypecast):boolean;
    function array_push(TT:TTypecast):boolean;
    //array_sum
    //array_avg
    //explode
    //implode
    //array_push
    //array_pop
    //array_shift
    //array_slice
    //sort, ksort
    //array_fill
    //array_key_exists
    //array_reverse
    //array_walk
    function copy(Value: TTypecast): TTypecast;
    function merge(Value: TTypecast): TTypecast;
    function unset(Index: Variant): TTypecast;
    
end;

function typecast_from_json(var tt:TTypecast; json:ansistring):boolean;
function typecast_save_to_file(tt:TTypecast; fname:ansistring):boolean;

function typecast_find(key: string; tt:TTypeCast):TTypecast;
function typecast_format(s: string; tt, vars:TTypeCast):string;

implementation

    constructor TTypecast.Create();
    begin
      //inherited Create;
      owner:=nil;
      confirmed:=false;
      val.ptr:=nil;
      val.size:=0;
      val.cast:=0;
      fReadCallback:=nil;
      fWriteCallback:=nil;
    end;
    
    destructor TTypecast.Destroy();
    begin
      //writeln('DESTROY ', key, ' ', val.cast);
      clear();
      //writeln('CLEARED');
      inherited Destroy();
      //writeln('DESTROYED'); 
    end;
    
    procedure TTypecast.setOwner(o:TTypecast);
    begin
      owner:=o;
    end;

    function TTypecast.isSet():boolean;
    begin
      result:=val.cast > 0; 
    end;
    
    function TTypecast.isEmpty():boolean;
    begin
      result:=val.cast = 0; 
    end;
    
    function TTypecast.isArray():boolean;
    begin
      result:=val.cast = 90; 
    end;

    function TTypecast.isIndexedArray():boolean;
    var i:integer;
    begin
      result:=false;
      if (val.cast = 90) then
      begin
        for i:=0 to length(Self.Values)-1 do
        begin
          if not (variants.varType(Self.Values[i]._key_) in
            [varShortInt, varSmallint, varInteger, varInt64, varByte, varWord, varLongWord, varQWord]
          ) then exit;
          if integer(Self.Values[i]._key_) <> i then exit;
        end;
      end;
      result:=true;
    end;
    
    function TTypecast.isBlob():boolean;
    begin
      result:=val.cast = 100;
    end;

    function TTypecast.blockwrite(buffer:pointer;size:longint;blocksize:integer):boolean;
    var i,lastsize:longint;
    var p:pointer;
    begin
      if val.cast <> 100 then
      begin
        clear();
        val.size:=size;
        val.ptr:=getmem(val.size);
        val.mode:=1;
        val.cast:=100;
        lastsize:=0;
      end else begin
        lastsize:=val.size;
        val.size:=val.size + size;
        reAllocMem(val.ptr, val.size);
      end;
      p:=val.ptr + lastsize;
      for i:=0 to size - 1 do
      begin
        byte(pointer(p + i)^):=byte(pointer(buffer + i)^);
      end;
      WriteCallback();
      result:=true;   
    end;
    
    function TTypecast.blockread(buffer:pointer;size:longint;blocksize:integer):boolean;
    var i,offset:longint;
    var p:pointer;
    begin
      offset:=0;
      p:=val.ptr + offset;
      for i:=0 to size - 1 do
      begin
        byte(pointer(buffer + i)^):=byte(pointer(p + i)^);
      end;    
    end;

    function TTypecast.print():boolean;
    var i, offset:longint;
    var p:pointer;
    var b:byte;
    begin
      result:=false;
      if val.size=0 then exit;
      offset:=0;
      p:=val.ptr + offset;
      for i:=0 to (val.size-1) do
      begin
        b:=byte(pointer(p + i)^);
        if b>0 then write(chr(b));
        if b=0 then begin
          writeln('[zero]');
          //byte(pointer(p + i)^):=ord('@');
          //break;
        end;
      end;
      writeln();
      result:=true;
    end;

    function TTypecast.printhex():boolean;
    var i, offset:longint;
    var p:pointer;
    begin
      result:=false;
      if val.size=0 then exit;
      offset:=0;
      p:=val.ptr + offset;
      for i:=0 to (val.size-1) do
      begin
        write('$' + inttohex(byte(pointer(p + i)^), 2) + ' ');
      end;
      writeln();
      result:=true;
    end;

    function TTypecast.convertToString():boolean;
    //var pp:pchar;
    begin
      result:=false;
      //writeln('CONVERTTOSTRING');
      if self.isBlob() and self.isChars() then
      begin
        if not self.isZeroTerminated() then
        begin
          self.zeroTerminate();
        end;
        //writeln('STRLEN', Strlen(pchar(val.ptr)));
        //PP:=StrAlloc(Strlen(pchar(val.ptr))+1);
        //STrCopy (PP,pchar(val.ptr));
        //self._str(AnsiString(pchar(val.ptr)));
        val.mode:=1;
        val.cast:=30; 
        result:=true;          
      end;
    end;

    function TTypecast.isChars():boolean;
    var i:longint;
    begin
      result:=true;
      //writeln('isChars');
      for i:=0 to val.size - 2 do
      begin
        if byte(pointer(val.ptr + i)^)=0 then
        begin
          //writeln('[zero]');
          result:=false;
        end;
      end;
    end;
    
    function TTypecast.isZeroTerminated():boolean;
    begin
      result:=false;
      if byte(pointer(val.ptr + (val.size - 1))^)=0 then
        begin
          result:=true;
        end;
    end;
    
    function TTypecast.zeroTerminate():boolean;
    begin
      val.size:=val.size + 1;
      reAllocMem(val.ptr, val.size);
      byte(pointer(val.ptr + (val.size - 1))^):=0;
    end;
    
    function TTypecast.size_(): longint;
    begin
      result:=self.val.size;
    end;
    
    function TTypecast.merge(Value: TTypecast): TTypecast;
    var row:TTypeCast=nil;
    begin
      result:=self;
      if value.cast_() <> 90 then exit;
      if (self.cast_() <> 0) and (self.cast_() <> 90) then
      begin
        self._cast(90);
      end; 
      value.reset(row);
      while value.each(row) do
      begin
        //writeln(row.iter, ' ', row._key_, ' ', TTypecast(row)._str_);
        self[row._key_].copy(row);
      end;
      row.free();
    end;

    function TTypecast.copy(Value: TTypecast): TTypecast;
    var p:pointer;
    begin
      result:=self;
      case value.cast_() of
        1: self._bool(value.bool_());//result:='boolean';
        10: self._int(value.int_());//result:='integer';
        20: self._float(value.float_());//result:='real';
        //30: self._str(value.str_());//result:='string';
        50: self._pchar(value.pchar_());//result:='pchar';
        60: self._ptr(value.ptr_());//result:='pointer';
        90: begin
          self.clear();
          self.merge(value);
        end;
        100: begin
          self.clear();
          p:=getmem(value._size_);
          value.blockread(p, value._size_, 0);
          self.blockwrite(p, value._size_, 0);
          freemem(p, value._size_);
        end;
        else begin
          self.clear();
          p:=getmem(value._size_);
          value.blockread(p, value._size_, 0);
          self.blockwrite(p, value._size_, 0);
          freemem(p, value._size_); 
          self._cast_:=value._cast_;       
        end;
      end;
      //self._key_:=value._key_;
      self.setSetup(value.getSetup());
    end;

    function TTypecast.unset(Index: Variant): TTypecast;
    var Cursor, i: Integer;
    var row:TTypeCast=nil;
    var tt:ttypecast;
    begin
      result:=self;
      Cursor := Self._Index_(Index);
      if cursor<0 then exit;
      tt:=TTypecast.Create();
      self.reset(row);
      while self.each(row) do
      begin
        //writeln(indent, row.idx, ' ', row.key, ' ', TTypecast(row.value)._str_);
        if (row.iter <> cursor) then
        begin
          tt[row._key_].copy(row);
        end;
      end;
      row.free();
      self.copy(tt);
      tt.free();
    end;

    procedure TTypecast._cast(Value: integer);
    var tt:TTypecast;
    begin
      case value of
        1: self._bool(self.bool_());//result:='boolean';
        10: self._int(self.int_());//result:='integer';
        20: self._float(self.float_());//result:='real';
        30: begin
          if val.cast = 100 then
          begin
            if self.convertToString() then exit;
          end;
          self._str(self.str_());//result:='string';
        end;
        50: self._pchar(self.pchar_());//result:='pchar';
        60: self._ptr(self.ptr_());//result:='pointer';
        90: begin
          if val.cast <> 90 then
          begin
            tt:=TTypecast.Create();
            tt.Copy(self);
            self._fld(0, tt);
          end;
        end;//result:='array';
        100: begin
          if val.cast = 90 then
          begin
            self.clear();
          end;
          val.cast := 100;
        end;//result:='blob';
      end;
    end;
    
    function TTypecast.cast_(): integer;    
    begin
      result:=val.cast; 
    end;

    procedure TTypecast._type(Value: string);
    begin
      case value of
        'bool':_cast(1);
        'int':_cast(10);
        'float':_cast(20);
        'str':_cast(30);
        'pchar':_cast(50);
        'ptr':_cast(60);
        'array':_cast(90);
        'blob':_cast(100);
      end;      
    end;

    function TTypecast.type_(): string;    
    begin
      result:='undefined';
      case val.cast of
        1:   result:='bool';
        10:  result:='int';
        20:  result:='float';
        30:  result:='str';
        50:  result:='pchar';
        60:  result:='ptr';
        90:  result:='array';
        100: result:='blob';
      end; 
    end;
    
    function TTypecast.isString():boolean;
    begin
      result:=val.cast = 30; 
    end;
    
    function TTypecast.isBoolean():boolean;
    begin
      result:=val.cast = 1; 
    end;
    
    function TTypecast.isInteger():boolean;
    begin
      result:=val.cast = 10; 
    end;
    
    function TTypecast.isReal():boolean;
    begin
      result:=val.cast = 20; 
    end;

    function TTypecast.isPchar():boolean;
    begin
      result:=val.cast = 50; 
    end;
    
    function TTypecast.isPointer():boolean;
    begin
      result:=val.cast = 60; 
    end;
        
    procedure TTypecast.clear();
    var row:TTypeCast=nil;
    begin
      if (iSetup and 32) > 0 then write('Clear ');
      if (val.ptr <> nil) then
      begin
        //writeln('freemem ', self._key_, ' ', val.cast, ' ', val.size);
        if (val.size > 0) then
        begin
          
          if (iSetup and 32) > 0 then write(val.cast, ' ', val.size);
          //if val.cast<>60 then
          //begin
            //writeln('fm');
            FreeMem(val.ptr, val.size);
          //end;
          val.size:=0;
          
        end;
        val.ptr:=nil;
        val.cast:=0;
        //writeln('/freemem ');
      end;
      //writeln('CLEAR:reset');
      if self.isArray() then
      begin
        self.reset(row);
        while self.each(row) do
        begin
          //writeln('free ', row.iter, ' | ', self[row.iter]._key_);

          self[row.iter].free();
        end;
        //writeln('CLEAR:after');
        row.free();
      end;
      //writeln('CLEAR:free');
      SetLength (Self.Values, 0);

      confirmed:=false;     
      if (iSetup and 32) > 0 then writeln();
      //writeln('/');
    end;
    
    procedure TTypecast.setSetup(i: integer);
    begin
      iSetup:=i;
    end;

    function TTypecast.getSetup():integer;
    begin
      result:=iSetup;
    end;
{    
    procedure TTypecast.setReadCallback(f: pTypecastCallback);
    begin
      fReadCallback:=f;
    end;
    
    procedure TTypecast.setWriteCallback(f: pointer);
    begin
      fWriteCallback:=pTypecastCallback(f);
    end;
}
    procedure TTypecast._idx(Value: Longint);
    begin
      self.idx:=Value;
    end;
    
    function TTypecast.idx_(): Longint;
    begin
      result:=self.idx;
      if fReadCallback <> nil then fReadCallback^(self);
    end;

    procedure TTypecast._key(Value: Variant);
    begin
      self.key:=Value;
    end;
    
    function TTypecast.key_(): Variant;
    var s:string;
    begin
      //write('.');
      //self._dumpType(self.key);
      Case variants.varType(self.key) of
        varShortInt, varSmallint, varInteger, varInt64, varByte, varWord, varLongWord, varQWord: begin
          result:=integer(self.key);        
        end;
        varString, varOleStr: begin
          s:=string(self.key);
          result:=system.copy(s,1,length(s));        
        end;
      end;

      //write('.');
      //if fReadCallback <> nil then fReadCallback^(self);
    end;

    function TTypecast.keystr_(): string;
    var s:string;
    begin
      Case variants.varType(self.key) of
        varShortInt, varSmallint, varInteger, varInt64, varByte, varWord, varLongWord, varQWord: begin
          result:=inttostr(self.key);        
        end;
        varString, varOleStr: begin
          s:=string(self.key);
          result:=system.copy(s,1,length(s));        
        end;
      end;
    end;

    function TTypecast.owner_(): Variant;
    begin
      result:='';
      if owner<>nil then result:=owner._key_;
    end;

    procedure TTypecast._bool(Value: Boolean);
    begin
      clear();
      val.size:=sizeof(Boolean);
      getmem(val.ptr, val.size);
      Boolean(val.ptr^):=Value;
      val.mode:=1;
      val.cast:=1;
      WriteCallback();
    end;
    
    function TTypecast.bool_(): Boolean;
    begin
      result:=false;
      if val.cast = 1 then result:=Boolean(val.ptr^);
      if fReadCallback <> nil then fReadCallback^(self);
    end;

    procedure TTypecast._int(Value: Integer);
    begin
      clear();
      val.size:=sizeof(Integer);
      getmem(val.ptr, val.size);
      Integer(val.ptr^):=Value;
      val.mode:=1;
      val.cast:=10;    
      WriteCallback();
    end;
    
    function TTypecast.int_(): Integer;
    begin
      result:=0;
      if val.cast = 10 then result:=Integer(val.ptr^);
      if fReadCallback <> nil then fReadCallback^(self);    
    end;

    procedure TTypecast._float(Value: float);
    begin
      clear();
      val.size:=sizeof(float);
      getmem(val.ptr, val.size);
      Float(val.ptr^):=Value;
      val.mode:=1;
      val.cast:=20;    
      WriteCallback();
    end;
    
    function TTypecast.float_(): Float;
    begin
      result:=0;
      if val.cast = 20 then result:=Float(val.ptr^);
      //result:=float(round(result*1000));//1000;
      if fReadCallback <> nil then fReadCallback^(self);    
    end;

    procedure TTypecast._str(Value: AnsiString);
    begin
      clear();
      //val.size:=sizeof(Value);
      //getmem(val.ptr, val.size);
      val.size:=length(Value)+1;
      val.ptr:=Strings.strAlloc(val.size);
      //String(val.ptr^):=Value;
      Strings.strPCopy(val.ptr, value);
      val.mode:=1;
      val.cast:=30;  
      WriteCallback();
    end;

    function TTypecast.var_(): variant;
    begin
      if self.isBoolean() then begin
        result:=self.bool_();
      end else if self.isInteger() then begin
        result:=self.int_();
      end else if self.isReal() then begin
        result:=self.float_();
      end else begin
        result:=self.str_();
      end;
    end;
    
    function TTypecast.str_(): AnsiString;
    begin
      result:='';
      case val.cast of
        1: if Boolean(val.ptr^) then result:='TRUE' else result:='FALSE';
        10: result:=IntToStr(Integer(val.ptr^));
        //30: result:=String(val.ptr^);
        30: begin
          //result:=Strings.StrPas(val.ptr);
          result:=AnsiString(pchar(val.ptr));
        end;
        50: begin
          //result:=Strings.StrPas(val.ptr);
          result:=AnsiString(pchar(val.ptr));
        end;
        60: begin
          if self._ptr_ = nil then 
            result:='[NIL]'
          else
            result:='[PTR]';
        end;
        100: begin
            result:='[BLOB ' + inttostr(val.size) + ' bytes]';
        end;
      end;
      if fReadCallback <> nil then fReadCallback^(self);
    end;
    
    procedure TTypecast._pchar(Value: Pchar);
    begin
      clear();
      val.size:=0;
      //getmem(val.ptr, val.size);
      val.ptr:=Value;
      val.mode:=1;
      val.cast:=50;    
      WriteCallback();
    end;
    
    function TTypecast.pchar_(): Pchar;
    begin
      result:=nil;
      if val.cast = 50 then result:=val.ptr;
      if fReadCallback <> nil then fReadCallback^(self);    
    end;

    procedure TTypecast._ptr(Value: Pointer);
    begin
      clear();
      val.size:=0;
      //getmem(val.ptr, val.size);
      val.ptr:=Value;
      val.mode:=1;
      val.cast:=60;    
      WriteCallback();
    end;
    
    function TTypecast.ptr_(): Pointer;
    begin
      result:=nil;
      if val.cast = 60 then 
      begin
        result:=val.ptr;
      end else begin
        result:=val.ptr;
      end;
      if fReadCallback <> nil then fReadCallback^(self);    
    end;

    function TTypecast._dumpType(Index: Variant): Integer;
    begin
      write(Index, ' ');
      Case variants.varType(Index) of
        varEmpty:
          Writeln('Empty');
        varNull:
          Writeln('Null');
        varSingle:
          Writeln('Datatype: Single');
        varDouble:
          Writeln('Datatype: Double');
        varDecimal:
          Writeln('Datatype: Decimal');
        varCurrency:
          Writeln('Datatype: Currency');
        varDate:
          Writeln('Datatype: Date');
        varOleStr:
          Writeln('Datatype: UnicodeString');
        varStrArg:
          Writeln('Datatype: COM-compatible string');
        varString:
          Writeln('Datatype: Pointer to a dynamic string');
        varDispatch:
          Writeln('Datatype: Pointer to an Automation object');
        varBoolean:
          Writeln('Datatype: Wordbool');
        varVariant:
          Writeln('Datatype: Variant');
        varUnknown:
          Writeln('Datatype: unknown');
        varShortInt:
          Writeln('Datatype: ShortInt');
        varSmallint:
          Writeln('Datatype: Smallint');
        varInteger:
          Writeln('Datatype: Integer');
        varInt64:
          Writeln('Datatype: Int64');
        varByte:
          Writeln('Datatype: Byte');
        varWord:
          Writeln('Datatype: Word');
        varLongWord:
          Writeln('Datatype: LongWord');
        varQWord:
          Writeln('Datatype: QWord');
        varError:
          Writeln('ERROR determining variant type');
        else
          Writeln('Unable to determine variant type');
      end;
    end;

  function TTypecast._Index_ (Index: Variant): Integer;
  var Current,Records: Integer;
  begin
    Result := -1;
    Records := Length (Self.Values);
    if (iSetup and 32) > 0 then 
    begin
      self._dumpType(index);
    end;
    Case variants.varType(Index) of
      varShortInt, varSmallint, varInteger, varInt64, varByte, varWord, varLongWord, varQWord:
        if (Index >= 0) and (Index < Records) then result:=Index;
      varString, varOleStr:
        for Current:=0 to Records-1 do
        begin
          //writeln('kk ',Self.Values[Current]._key_);
          if Self.Values[Current]._key_=Index then
          begin
            Result := Current;
            break;
          end;
        end;   
    end;

  end;

  procedure TTypecast.WriteCallback();
  begin
    if (owner<>nil) and (not confirmed) then
    begin
      //writeln('confirmed, owner:', owner._key_, ' self:', self._key_, ' value:', self._str_);
      owner._fld(self._key_, self);
      
      //confirmed:=owner.insert(self);
    end;
  end;
 
  procedure TTypecast._fld (Index: Variant; Value: TTypecast);
  var Cursor: Integer;
  begin
    //Cursor := Self._Index_(tt._key_);
    //writeln('here');
    Cursor := Self._Index_(Index);
    //writeln('argh ', Index, ' ',Cursor, ' ', Count());
    if Cursor >- 1 then
    begin
      //Self.Values[Cursor].free();
      //Self.Values[Cursor]:= Value;
      //writeln('_fld!!! ', self._key_, ' => ', Cursor, ' ', Index, ' ', Value._str_);
      //writeln('exiting');
      exit;
    end;
    if (val.cast<>0) and (val.cast<>90) then
    begin
      clear();
    end;
    if (val.cast<>90) then
    begin
      val.cast:=90;
    end;
    //writeln('_fld ', self._key_, ' => ', Index, ' ', Value._str_);
    Cursor := Length(Self.Values);
    SetLength (Self.Values, Cursor+1);
    Self.Values[Cursor]:= Value;
    //value.confirmed:=true;
    WriteCallback();    
    //result:=true;
  end;

   
 function TTypecast.fld_(Index: Variant): TTypecast;
 var Cursor: Integer;
 begin
   Cursor := Self._Index_(Index);
   if Cursor=-1 then
   begin
     //writeln('create');
     result:=TTypecast.Create();
     result.setSetup(self.iSetup);
     result.setOwner(self);
     result._key_:=Index;
   end else begin
     //writeln('direct');
     Result := TTypecast(Self.Values[Cursor]);
     //Result.confirmed:=true;
   end;
 end;    

 function TTypecast.Count(): Integer;
 begin
   Result := Length (Self.Values)
 end;

  function TTypecast.Select(Index: Variant;var row: TIter): Boolean;
  begin
   if (self[Index].isSet()) then
   begin
     //writeln('-> ');
     row.idx:=self[Index]._idx_;
     row.key:=self[Index].key_();
     //writeln('oo'); writeln('key ', row.key);
     row.value:=TTypecast(self[Index]);
     Result := True;
   end
   else
   begin
     //writeln('ouch ', row.idx);
     Result := False;
     row.idx:=-1;
   end;
  end;

  procedure TTypecast.reset(var row: TTypeCast);
  begin
    if row=nil then row:=TTypeCast.create();
    row.iter:=-1;
  end;

  function TTypecast.Each(var row: TTypeCast): Boolean;
  begin
    inc(row.iter);
   //writeln('foreach ', row.idx);
    if (Self.Count()>0) and (row.iter >= 0) and (row.iter < Self.Count()) then
    begin
      //writeln('-> ');
      //row.clear();
      //row._cast_:=self._cast_;
      row.copy(Self[row.iter]);
      row._key_:=self[row.iter]._key_;
      row._idx_:=self[row.iter]._idx_;
      Result := True;
    end
    else
    begin
      //writeln('ouch ', row.idx);
      Result := False;
      //row.idx:=-1;
    end;    
  end;

  procedure TTypecast.ForeachStart(var row: TIter);
  begin
    row.idx:=-1;
  end;
  
  procedure TTypecast.ForeachEnd(var row: TIter);
  begin
    row.idx:=-1;
  end;

 function TTypecast.Foreach(var row: TIter): Boolean;
 begin
   writeln('FOREACH is deprecated! Use RESET and EACH instead.');
   inc(row.idx);
   //writeln('foreach ', row.idx);
   if (row.idx >= 0) and (row.idx < Self.Count()) then
   begin
     //writeln('-> ');
     row.key:=TTypecast(Self.values[row.idx]).key_();
     //writeln('oo'); writeln('key ', row.key);
     row.value:=TTypecast(Self.values[row.idx]);
     Result := True;
   end
   else
   begin
     //writeln('ouch ', row.idx);
     Result := False;
     row.idx:=-1;
   end;
 end;

 procedure TTypecast._dump(val1:TTypecast; indent:string);
 var row:TTypeCast=nil;
 begin
   val1.reset(row);
   while val1.each(row) do
   begin
     writeln(indent, row._idx_, ' ', row._key_, ' ', row._str_);
     if row.isArray() then
     begin         
       _dump(row, indent + '  ');
     end;
   end;
   row.free();
 end;

 procedure TTypecast.dump();
 begin
   if self.isArray() then
   begin
     _dump(self,  '');
   end else begin
     writeln('', self._idx_, ' ', self._key_, ' ', self._str_);
   end;
 end;

 function TTypecast._JSONA(val1:TTypecast):TJSONArray;
 var row:TTypeCast=nil;
 var json:TJSONArray;
 begin
   json:=TJSONArray.create();
   val1.reset(row);
   while val1.each(row) do
   begin
     //writeln(row.idx, ' ', row.key, ' ', TTypecast(row.value)._str_);
     if TTypecast(row).isString() then
       json.add(TTypecast(row)._str_)
     else if TTypecast(row).isInteger() then
       json.add(TTypecast(row)._int_)
     else if TTypecast(row).isReal() then
       json.add(TTypecast(row)._float_)
     else if TTypecast(row).isBoolean() then
       json.add(TTypecast(row)._bool_)
     else if TTypecast(row).isPointer() then
       json.add('[pointer]')
     else if TTypecast(row).isBlob() then
       json.add('[blob ' + inttostr(TTypecast(row)._size_) + ' bytes]')
     else if TTypecast(row).isIndexedArray() then
     begin         
       json.add(_JSONA(TTypecast(row)));
     end else if TTypecast(row).isArray() then
     begin         
       json.add(_JSONO(TTypecast(row)));
     end;
   end;
   row.free();
   result:=json;
 end;

 function TTypecast._JSONO(val1:TTypecast):TJSONObject;
 var row:TTypeCast=nil;
 var json:TJSONObject;
 begin
   json:=TJSONObject.create();
   val1.reset(row);
   while val1.each(row) do
   begin
     //writeln(row.idx, ' ', row.key, ' ', TTypecast(row.value)._str_);
     if TTypecast(row).isString() then
       json.add(row._key_, TTypecast(row)._str_)
     else if TTypecast(row).isInteger() then
       json.add(row._key_, TTypecast(row)._int_)
     else if TTypecast(row).isReal() then
       json.add(row._key_, TTypecast(row)._float_)
     else if TTypecast(row).isBoolean() then
       json.add(row._key_, TTypecast(row)._bool_)
     else if TTypecast(row).isPointer() then
       json.add(row._key_, '[pointer]')
     else if TTypecast(row).isBlob() then
       json.add(row._key_, '[blob ' + inttostr(TTypecast(row)._size_) + ' bytes]')
     else if TTypecast(row).isIndexedArray() then
     begin         
       json.add(row._key_, _JSONA(TTypecast(row)));
     end else if TTypecast(row).isArray() then
     begin         
       json.add(row._key_, _JSONO(TTypecast(row)));
     end;
   end;
   row.free();
   result:=json;
 end;
 
 function TTypecast.toJSON():string;
 var row:TTypeCast=nil;
 var jsono:TJSONObject;
 var jsona:TJSONArray;
 begin
   if self.isIndexedArray() then
   begin
     jsona:=_JSONA(self);
     jsona.CompressedJSON:=true;
     //result:=jsona.asJSON;
     result:=jsona.FormatJSON();
     jsona.free();
   end else if self.isArray() then
   begin
     jsono:=_JSONO(self);
     jsono.CompressedJSON:=true;
     //result:=jsono.asJSON;
     result:=jsono.FormatJSON();
     jsono.free();
   end else if self.isString() then
   begin
     result:='"' + self._str_ + '"';
   end else
   begin
     result:=self._str_;
   end;
 end;
 
 function TTypecast._JSONI(val1:TTypecast;jData : TJSONData):boolean;
   var
   jItem : TJSONData;
   i: integer;
   kkey:variant;
 begin
   for i := 0 to jData.Count - 1 do
   begin
     jItem := jData.Items[i];
     if jData.JSONType = jtArray then
     begin
       kkey:=i;
     end else begin
       kkey:=TJSONObject(jData).Names[i];
     end;
     //writeln('TYPE:', jItem.JSONType);
     //writeln('KEY:', kkey);
     if not (jItem.JSONType in [jtArray, jtObject]) then
     begin
       //writeln('VAL:', TJSONObject(jData).Items[i].asString);
     end;
     case jItem.JSONType of
       jtBoolean: begin
         val1[kkey]._bool_:=jItem.asBoolean;
       end;
       jtString: begin
         val1[kkey]._str_:=jItem.asString;
       end;
       jtNumber: begin
         if frac(jItem.asFloat) > 0  then
         begin
           //val1[kkey]._float_:=jItem.asFloat;
           val1[kkey]._str_:=FormatFloat('0.00', jItem.asFloat);
         end else begin
           val1[kkey]._int_:=jItem.asInteger;
         end;
       end;       
       jtArray: begin
         _JSONI(val1.fld_(kkey), jItem);
         //parse deeper...
       end;
       jtObject: begin
         _JSONI(val1.fld_(kkey), jItem);
         //parse deeper...
       end;
     end;

   end;
 
 end;

 function TTypecast.fromJSON(json:ansistring):boolean;
   var
   jData : TJSONData;
 begin
   jData := nil;
   result:=true;
   try
     jData := GetJSON(json);
     clear();
     _JSONI(self, jData);
    except
      on E: Exception do
      begin
        result:=false;
        //writeln( 'Error: '+ E.ClassName + #13#10 + E.Message );
      end;
   end;
   if jData<>nil then 
   begin
     jData.Free;
   end;
 end;

 function TTypecast.fromJSONpchar(json:pchar):boolean;
   var
   jData : TJSONData;
 begin
   jData := nil;
   result:=true;
   try
     jData := GetJSON(json);
     clear();
     _JSONI(self, jData);
    except
      on E: Exception do
      begin
        result:=false;
        //writeln( 'Error: '+ E.ClassName + #13#10 + E.Message );
      end;
   end;
   if jData<>nil then 
   begin
     jData.Free;
   end;
 end;

 
 function TTypeCast.expandJSON():boolean;
   var
     remember:ttypecast;
 begin
   result:=false;
   if self.isBlob() then
   begin
     self.convertToString();
   end else begin
     writeln('not blob');
   end;
   if self.isString() then
   begin
     writeln('is string');
     remember:=ttypecast.create();
     remember.copy(self);
     result:=self.fromJSON(self._str_);
     if result = false then
     begin
       writeln('failed, restoring...');
       self.copy(remember);
     end;
     remember.free();
   end else begin
     writeln('not string');
   end; 
 end;
 
    procedure TTypecast._json(s: ansistring);
    begin
      self.fromJSON(s);
    end;
    
    function TTypecast.json_(): ansistring;
    begin
      result:=self.toJSON();
    end;

  function TTypecast.array_keys():TTypecast;
  var i:integer;
  begin
   result:=TTypecast.Create();
    for i:=0 to length(Self.Values)-1 do
    begin
      result[i]._str_:=self.values[i]._key_;
    end;   
  end;

  function TTypecast.array_values():TTypecast;
  var i:integer;
  begin
   result:=TTypecast.Create();
    for i:=0 to length(Self.Values)-1 do
    begin
      result[i].copy(self.values[i]);
      result[i]._key_:=i;
    end;   
  end;

  function TTypecast.array_shift(var TT:TTypecast):boolean;
  begin
    self.reset(tt);
    if self.each(tt) then
    begin
      self.unset(tt.iter);
      result:=true;
    end else begin
      tt.free();
      tt:=nil;
      result:=false;
    end;
  end;
  
  function TTypecast.array_push(TT:TTypecast):boolean;
  var i:longint;
  begin
    i:=self.count();
    while self['!_'+inttostr(i)+'_'].isSet() do inc(i);
    //self[i]._int_:=0;
    self['!_'+inttostr(i)+'_'].copy(TT);
  end;
  
  function TTypecast.explode(delimiter:string):boolean;
  var remembersize:longint;
  var p0,p1,p2:pointer;
  var d,pp:pchar;
  var i,l,ll:longint;
  begin
    case val.cast of
      30: begin
        remembersize:=val.size;
        val.size:=0;
        d:=pchar(delimiter);
        L:=strlen(d);
        p0:=val.ptr;
        p1:=val.ptr;
        //result:=Strings.StrPas(val.ptr);
        i:=0;
        repeat
          p2:=StrPos(p1,d);
          if p2 = nil then 
          begin
            p2:=(p0+remembersize)-1;
            LL:=longint(p2)-Longint(p1);
            PP:=StrAlloc(LL+1);
            StrLCopy (PP,P1,LL);
            self[i]._str_:=strpas(PP);
            inc(i);
            StrDispose(PP);
            break;
          end;
          LL:=longint(p2)-Longint(p1);
          PP:=StrAlloc(LL+1);
          //Writeln ('Position of delimiter in P : ', LL);
          StrLCopy (PP,P1,LL);
          self[i]._str_:=strpas(PP);
          inc(i);
          StrDispose(PP);
          p1:=p2+L;//+1;
        until 6=9;
        //writeln('freeing ', longint(p0), ' ', remembersize);
        //writeln(pchar(p0));
        FreeMem(p0, remembersize);
      end;
    end;  
  end;
  
  function typecast_save_to_file(tt:TTypecast; fname:ansistring):boolean;
  var stream: TFileStream;
  var sl:TStringList;
  var s:ansistring;
  var i,l:longint;
  var a:pointer;
  begin
    //stream:=TFileStream.Create(fname, fmCreate);
    sl:=TStringList.Create();
    sl.add(tt._json_);
    sl.SaveToFile(fname);
    sl.free();
    exit;
    s:=tt._json_;
    L:=length(s);
    {
    a:=getmem(L);
    for i:=0 to L-1 do
    begin
      byte(a+i^):=ord(s[i]);
    end;
    }
    stream.write(s, L);
    freemem(a, L);
    stream.Free();
  end;
  
  function typecast_from_json(var tt:TTypecast; json:ansistring):boolean;

    Procedure DoParse(P : TJSONParser);
    Var J : TJSONData;
    begin
      Try
        J:=P.Parse;
        Try
          Writeln('Parse succesful. Dumping JSON data : ');
          If Assigned(J) then
            begin
            Writeln('Returned JSON structure has class : ',J.ClassName);
            Writeln(J.AsJSON)
            end
          else
            Writeln('No JSON data available');
        Finally
          FreeAndNil(J);
        end;
      except
        On E : Exception do
          Writeln('An Exception occurred when parsing : ',E.Message);
      end;
    end;
 
   function _JSONI(val1:TTypecast;jData : TJSONData):boolean;
     var jItem : TJSONData;
     var i: integer;
     var kkey:variant;
     var ustr:unicodestring;
   begin
     for i := 0 to jData.Count - 1 do
     begin
       jItem := jData.Items[i];
       if jData.JSONType = jtArray then
       begin
         kkey:=i;
       end else begin
         kkey:=TJSONObject(jData).Names[i];
       end;
       //writeln('TYPE:', jItem.JSONType);
       //writeln('KEY:', kkey);
       if not (jItem.JSONType in [jtArray, jtObject]) then
       begin
         //writeln('VAL:', TJSONObject(jData).Items[i].asString);
       end;
       case jItem.JSONType of
         jtBoolean: begin
           val1[kkey]._bool_:=jItem.asBoolean;
         end;
         jtString: begin
           //val1[kkey]._str_:=AnsiString(jItem.asUnicodeString);
           val1[kkey]._str_:=AnsiString(jItem.asString);

         end;
         jtNumber: begin
           val1[kkey]._float_:=jItem.asFloat;
           if frac(jItem.asFloat) = 0  then
           begin
             val1[kkey]._int_:=jItem.asInteger;
           end;
           //val1[kkey]._int_:=jItem.asInteger;
         end;       
         jtArray: begin
           _JSONI(val1.fld_(kkey), jItem);
           //parse deeper...
         end;
         jtObject: begin
           _JSONI(val1.fld_(kkey), jItem);
           //parse deeper...
         end;
       end;
     end;
   end;

   var jData : TJSONData;
   var stream: TStream;
   Var P : TJSONParser;
  begin
    {
    P:=TJSONParser.Create(json);
    try
      DoParse(P);
    finally
      FreeAndNil(P);
    end;
    exit; 
   }
   jData := nil;
   stream:= nil;
   result:= true;
   try
     if fileexists(json) then
     begin
       stream:=TFileStream.Create(json, fmOpenRead);
     end else begin
       stream:=TStringStream.Create(json + chr(0));
     end;
     //writeln('stream.size: ', stream.size);
     //stream.Write(json);
     jData := GetJSON(TStream(stream));
     //writeln('oo?', jData.Count);
     
     tt.clear();
     _JSONI(tt, jData);
    except
      on E: Exception do
      begin
        result:=false;
        //writeln( 'Error: '+ E.ClassName + #13#10 + E.Message );
      end;
   end;
   if jData<>nil then 
   begin
     jData.Free;
   end;
   if stream<>nil then 
   begin   
     stream.Free();
   end;
 end;
 
  function typecast_find(key: string; tt:TTypeCast):TTypecast;
  var kk, cc:TTypeCast;
  var k:variant;
  var i:byte;
  begin
    kk:=TTypeCast.Create();
    kk._str_:=key;
    kk.explode('.');
    cc:=tt;
    for i:=0 to kk.Count()-1 do
    begin
      k:=kk[i]._str_;
      if cc[k].isSet() then
      begin
        cc:=cc[k];
      end else begin
        result:=nil;
        exit;
      end;
    end;
    kk.Free();
    result:=cc;
  end;

  function typecast_format(s: string; tt, vars:TTypeCast):string;
  var a:array of string;
  var i:byte;
  var tc:TTypeCast;
  var k:variant;
  var ss:string;
  begin
    setLength(a, tt.count());
    ss:=s;
    for i:=0 to tt.Count()-1 do
    begin
      k:=tt[i]._str_;
      tc:=typecast_find(k, vars);
      if tc = nil then
      begin
        a[i]:='';
      end else begin
        a[i]:=tc._str_;
      end;
      ss:=stringreplace(ss, '{{' + k + '}}', a[i], [rfReplaceAll]);
    end;
    result:=ss;  
  end;
    
end.    
