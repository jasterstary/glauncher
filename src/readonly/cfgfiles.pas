unit CFGfiles;
{
!REPOSITORY!
!NAME: CFGfiles
!VERSION: 0.9.2
!AUTHOR: JasterStary
!DATE: 2006-01-01
!DESCRIPTION: unit with parsing algorithms
!HISTORY: 2009-03-22;JasterStary;Merged with variant, added convertnumber2,real2,boolean,parsesupesmart
!HISTORY: 2008-10-20;JasterStary;Fixed convert number overflow.
}
interface

{Qdefine timecode}

uses Sysutils
{$ifdef timecode},cinemath{$endif}
,regexpr
;

function parsingCfg(sign:string;var sline:string):boolean;
function parsingCfgLCase(sign:string;var sline:string):boolean;
function parsingVcardLCase(sign:string;var a,sline:string):boolean;
function parsingCmd(sign:string;var sline:string):boolean;
function parsingCom(sign:string;var sline:string;delim:string):boolean;
function convertnumber(sline:string;min,max,def:cardinal):cardinal;
function convertnumber2(sline:string;min,max,def:integer;var serr:string):integer;
//function convertreal2(sline:string;min,max,def:real):real;
function convertboolean(sline:string;def:boolean):boolean;

//function parsingCfgRegexp(sign:string;var sline:string;case_insens:boolean):boolean;
//function parsingCfgRegexpPre(sign:string;var sline:string;case_insens:boolean):boolean;

function parsePhrases(var sline:string):string;
function parseSubPhrases(var sline:string):string;
function parseWords(var sline:string):string;
function isAsk(var sline:string):boolean;
function isExclam(var sline:string):boolean;
function isDot(var sline:string):boolean;
function isComa(var sline:string):boolean;
function isSemicolon(var sline:string):boolean;

{$ifdef timecode}
function convert_tc_srt(sline:string):ttimecode;

{$endif}
function convertreal(sline:string;min,max,def:real):real;
function convert_dd_mm_yy_2_yyyy(sline:string):string;
function parsestr(var sline:string;var s,r,d:string):boolean;
function parsestr2(var sline:string;var a,b:string):boolean;
function parsestr3(var sline:string;var a,b,c:string):boolean;
function parsestr4(var sline:string;var a,b,c,d:string):boolean;
function parsesimple(var sline:string;var a,b:string):boolean;
function parsesmart(var sline:string;d:char):string;
function parsesmarter(var sline:string;d:char):string;
function parsesupersmart(var sline:string;d:char):string;

function clearWhiteSpace(a:string):string;
implementation


{****************************************************************************
DRAK proudly presents:
function:...... Str2real
declaration:... function Str2real(flt:string):real;
note:..........
zoerie ?ovek-p?en?ypuje, ?lice pouje, iadne vnimky nerob?
Ak je prv??eno -, potom je vsledok m?s. Desatinn?iarka me by
aj na prvom mieste, poui mono . alebo , poda chuti. Ak sa omylom vlo?viac desatinnch ?rok, plat?rv? nich.
date:.......... 11.07.2003
author:........ Drak
revisited:.....
*****************************************************************************}
function Str2real(flt:string):real;
const decimals:array[1..14]of extended=(0.000001,0.00001,0.0001,0.001,0.01,0.1,1,10,100,1000,10000,100000,1000000,10000000);
      ONE=7;
var nums:array[0..100]of byte;
    plus,coma:boolean;
    i,x:byte;
    frac:integer;
    ch:Pchar;
    numero:extended;
begin
  plus:=true;frac:=0;x:=0;coma:=false;
  for i:=0 to 15 do nums[i]:=0;
  if length(flt)<0 then begin result:=0;exit;end;
  ch:=StrAlloc( length(flt)+1);
  StrPCopy(ch,flt);
  if ch[0]='-' then plus:=false else
  if ch[0]='+' then plus:=true else
  if ch[0] in ['.',','] then coma:=true else
  if ch[0] in ['0'..'9'] then nums[0]:=StrToInt(ch[0]);

  if length(flt)>1 then
  begin
    for i:=1 to length(flt) do
    begin
      if ch[i] in ['0'..'9'] then
      begin
        inc(x);
        nums[x]:=StrToInt(ch[i]);
        if not coma then inc(frac);
      end;
      if ch[i] in [',','.'] then coma:=true;
      if (x>12) or (i>20) then break;
    end;
  end;
  numero:=0;
  for i:=0 to x do
  begin
    if(((frac+ONE)<=high(decimals))and((frac+ONE)>=low(decimals)))then
      if((i<=high(nums))and(i>=low(nums)))then
    numero:=numero+(nums[i]*decimals[frac+ONE]);
    dec(frac);
  end;
  if not plus then
  result:=numero*(-1) else result:=numero;
  strdispose(ch);
end;

function clearWhiteSpace(a:string):string;
var aa:string;p,i:integer;
begin
aa:=a;p:=0;
for i:=1 to length(a) do
begin
  if aa[i]=' ' then p:=i else break;
end;
if p<>0 then delete(aa,1,p);
p:=0;
for i:=length(aa) downto 1 do
begin
  if aa[i]=' ' then p:=i else break;
end;
if p<>0 then delete(aa,p,length(aa));
result:=aa;
end;

function parsingCfg(sign:string;var sline:string):boolean;
var p: integer;
begin
result:=false;
if pos(sign,sLine)<>1 then exit;
if (copy(sLine,1,pos(' ',sLine)-1)=sign)or(sLine=sign) then
   begin
     delete(sline,1,pos(' ',sLine));
     p:=pos('#',sLine);
     if p>0 then delete(sline,p,(length(sLine)+1)-p);
     result:=true;
     //write(sign+'*');//write(sline);readln;
   end else result:=false;
end;

function parsingCfgLCase(sign:string;var sline:string):boolean;
var p: integer;
begin
result:=false;
if pos(sign,lowercase(sLine))<>1 then exit;
if (copy(sLine,1,pos(' ',lowercase(sLine))-1)=sign)or(lowercase(sLine)=sign) then
   begin
     delete(sline,1,pos(' ',sLine));
     p:=pos('#',sLine);
     if p>0 then delete(sline,p,(length(sLine)+1)-p);
     result:=true;
     //write(sign+'*');//write(sline);readln;
   end else result:=false;
end;
{
function parsingCfgRegexp(sign:string;var sline:string;case_insens:boolean):boolean;
var p,l: integer;
var reg:TRegExprEngine;
var res:boolean;
begin
result:=false;
if case_insens then res:=GenerateRegExprEngine(pchar(sign),[ref_caseinsensitive],reg) else res:=GenerateRegExprEngine(pchar(sign),[],reg);
if not res then begin writeln('error: '+sign);exit;end;
if not regexprpos(reg,pchar(sline),p,l)then begin DestroyRegExprEngine(reg);exit;end;
DestroyRegExprEngine(reg);
     delete(sline,1,p+l);
     sLine:=clearWhiteSpace(sLine);
     p:=pos('#',sLine);
     if p>0 then delete(sline,p,(length(sLine)+1)-p);
     result:=true;
     //write(sign+'*');//write(sline);readln;
end;

function parsingCfgRegexpPre(sign:string;var sline:string;case_insens:boolean):boolean;
var p,l: integer;
var reg:TRegExprEngine;
var res:boolean;
begin
result:=false;
if case_insens then res:=GenerateRegExprEngine(pchar(sign),[ref_caseinsensitive],reg) else res:=GenerateRegExprEngine(pchar(sign),[],reg);
if not res then begin writeln('error: '+sign);exit;end;
if not regexprpos(reg,pchar(sline),p,l)then begin DestroyRegExprEngine(reg);exit;end;
DestroyRegExprEngine(reg);
     delete(sline,p,length(sline));
     sLine:=clearWhiteSpace(sLine);
     result:=true;
     //write(sign+'*');//write(sline);readln;
end;
}
function parsingVcardLCase(sign:string;var a,sline:string):boolean;
var p: integer;
begin
  result:=false;
  a:='';
  if pos(':',sLine)<1 then exit;
  a:=copy(sline,1,pos(':',sLine)-1);
  for p:=1 to length(a) do if a[p]=';' then a[p]:='_';
  if pos(sign,a)<>1 then exit;
  writeln(a+'-->'+sign);
  if (a=sign) then
    begin
      delete(sline,1,pos(':',sLine));
      for p:=1 to length(sline) do if sline[p]=';' then sline[p]:=' ';
      writeln('____________- '+sline);
      result:=true;
    end else result:=false;
end;

function parsingCmd(sign:string;var sline:string):boolean;
var p: integer;
begin
result:=false;
if pos(sign,sLine)<>1 then exit;
if (copy(sLine,1,pos('=',sLine))=sign)or(sLine=sign) then
   begin
     delete(sline,1,pos('=',sLine));
     p:=pos('#',sLine);
     if p>0 then delete(sline,p,(length(sLine)+1)-p);
     result:=true;
     //write(sign+'*');//readln;
   end else result:=false;
end;

function parsingCom(sign:string;var sline:string;delim:string):boolean;
var p: integer;
begin
result:=false;
if pos(sign,sLine)<>1 then exit;
if (copy(sLine,1,pos(delim,sLine))=sign)or(sLine=sign) then
   begin
     delete(sline,1,pos(delim,sLine));
     p:=pos('#',sLine);
     if p>0 then delete(sline,p,(length(sLine)+1)-p);
     result:=true;
     //write(sign+'*');//readln;
   end else result:=false;
end;

function convertnumber(sline:string;min,max,def:cardinal):cardinal;
var v,c: integer;
begin
result:=def;
for v:=1 to length(sLine)do
if not(sLine[v]in['1'..'9','0','+','-'])then delete(sLine,v,1);
if(sLine='')then exit;if(sLine='+')then exit;if(sLine='-')then exit;
v:=0;
val(sLine,c,v);
if v=0 then
   begin
     if (c>=min) and (c<(max+1)) then
     result:=c;
   end else begin write('err',v,sLine);readln;end;
end;

function convertreal(sline:string;min,max,def:real):real;
var v: integer;c:real;
begin
  result:=def;
  c:=Str2real(sLine);
  if (c>=min) and (c<(max+1)) then
  result:=c
  else begin writeln('err'); result:=def; end;
end;
{
function convertreal(sline:string;min,max,def:single):single;
var v,u,c,d: integer;sLineA,sLineB:string;r:real;
begin
result:=def;u:=0;
if pos('.',sLine)>0 then begin
sLineA:=copy(sLine,1,pos('.',sLine)-1);
sLineB:=copy(sLine,(pos('.',sLine))+1,length(sLine)-pos('.',sLine));
val(sLineA,c,v);
val(sLineB,d,u);
writeLn(sLineA+' '+sLineB);
r:=c+(d*0.1);
end else
begin
val(sLine,c,v);
r:=c;
end;
if (v=0) and (u=0) then
   begin
     if (r>=min) and (r<(max+1)) then
     result:=r;
   end else begin write('err',v,'/',u);readln;end;
end;
}
function parsestr(var sline:string;var s,r,d:string):boolean;
begin
  result:=false;
  s:='';r:='';d:='';
  s:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  if pos(';',sLine)=0 then exit;
  r:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  if pos(';',sLine)=0 then exit;
  d:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  result:=true;
  //write(s,r,d);readln;
end;

function parsestr2(var sline:string;var a,b:string):boolean;
begin
  result:=false;
  a:='';b:='';
  a:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  if pos(';',sLine)=0 then exit;
  b:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  result:=true;
end;

function parsestr3(var sline:string;var a,b,c:string):boolean;
begin
  result:=false;
  a:='';b:='';c:='';
  a:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  if pos(';',sLine)=0 then exit;
  b:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  if pos(';',sLine)=0 then exit;
  c:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  result:=true;
  //write(s,r,d);readln;
end;

function parsestr4(var sline:string;var a,b,c,d:string):boolean;
begin
  result:=false;
  a:='';b:='';c:='';d:='';
  a:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  if pos(';',sLine)=0 then exit;
  b:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  if pos(';',sLine)=0 then exit;
  c:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  if pos(';',sLine)=0 then exit;
  d:=copy(sLine,1,pos(';',sLine)-1);
  delete(sline,1,pos(';',sLine));
  result:=true;
end;

function parsesimple(var sline:string;var a,b:string):boolean;
begin
  result:=false;
  a:='';b:='';
  if pos(' ',sLine)<1 then begin a:=sline;exit;end;
  a:=copy(sLine,1,pos(' ',sLine)-1);
  delete(sline,1,pos(' ',sLine));
  b:=sLine;
  result:=true;
end;

function parsesmart(var sline:string;d:char):string;
begin
  result:='';
  if pos(d,sLine)<1 then begin result:=sline;exit;end;
  result:=copy(sLine,1,pos(d,sLine)-1);
  delete(sline,1,pos(d,sLine));
end;

function parsesmarter(var sline:string;d:char):string;
begin
  result:='';
  if pos(d,sLine)<1 then begin result:=sline;sline:='';exit;end;
  result:=copy(sLine,1,pos(d,sLine)-1);
  delete(sline,1,pos(d,sLine));
end;

function parsesupersmart(var sline:string;d:char):string;
var runningstr:boolean;st:string;a,b:integer;
begin
  result:='';runningstr:=false;a:=0;
  if pos('"',sLine)=1 then begin runningstr:=true;delete(sline,1,1);a:=pos('"',sLine);end;
  b:=pos(d,sLine);
  if b<1 then begin result:=sline;exit;end;
  if a>0 then
    begin st:=copy(sLine,1,a-1);delete(sline,1,a);end
  else
    begin st:=copy(sLine,1,pos(d,sLine)-1);end;
  delete(sline,1,pos(d,sLine));//aj tak.
  result:=st;
end;

function parsePhrases(var sline:string):string;
var d:char;p,p1,p2,p3,p4:integer;
begin
  result:='';
  p1:=pos('.',sLine);
  p2:=pos('?',sLine);
  p3:=pos('!',sLine);
  p:=length(sline);
  //write(p, ' ' , p1, ' ', p2, ' ', p3,' ');
  if (p1>0)and(p1<p) then p:=p1;
  if (p2>0)and(p2<p) then p:=p2;
  if (p3>0)and(p3<p) then p:=p3;
  //writeln(p);
  if p<1 then begin result:=sline;sline:='';exit;end;
  result:=clearwhitespace(copy(sLine,1,p));
  delete(sline,1,p);
end;

function parseSubPhrases(var sline:string):string;
var d:char;p,p1,p2,p3,p4:integer;
begin
  result:='';
  p1:=pos(',',sLine);
  p2:=pos(';',sLine);
  p:=length(sline);
  if (p1>0)and(p1<p) then p:=p1;
  if (p2>0)and(p2<p) then p:=p2;
  if p<1 then begin result:=sline;sline:='';exit;end;
  result:=clearwhitespace(copy(sLine,1,p));
  delete(sline,1,p);
end;

function parseWords(var sline:string):string;
var d:char;p,p1,p2,p3,p4:integer;
begin
  result:='';
  p1:=pos(' ',sLine);
  p2:=pos(#9,sLine);
  p3:=pos(#10,sLine);
  p:=length(sline);
  if (p1>0)and(p1<p) then p:=p1;
  if (p2>0)and(p2<p) then p:=p2;
  if (p3>0)and(p3<p) then p:=p3;
  if p<1 then begin result:=sline;sline:='';exit;end;
  result:=clearwhitespace(copy(sLine,1,p));
  delete(sline,1,p);
end;

function isAsk(var sline:string):boolean;
begin
  sline:=clearwhitespace(sline);result:=false;//writeln('XXXXX:'+sline[length(sline)]);
  if(sline[length(sline)])='?' then begin result:=true;delete(sline,length(sline),1);end else exit;
  sline:=clearwhitespace(sline);
end;

function isExclam(var sline:string):boolean;
begin
  sline:=clearwhitespace(sline);result:=false;
  if(sline[length(sline)])='!' then begin result:=true;delete(sline,length(sline),1);end else exit;
  sline:=clearwhitespace(sline);
end;

function isDot(var sline:string):boolean;
begin
  sline:=clearwhitespace(sline);result:=false;
  if(sline[length(sline)])='.' then begin result:=true;delete(sline,length(sline),1);end else exit;
  sline:=clearwhitespace(sline);
end;

function isComa(var sline:string):boolean;
begin
  sline:=clearwhitespace(sline);result:=false;
  if(sline[length(sline)])=',' then begin result:=true;delete(sline,length(sline),1);end else exit;
  sline:=clearwhitespace(sline);
end;

function isSemicolon(var sline:string):boolean;
begin
  sline:=clearwhitespace(sline);result:=false;
  if(sline[length(sline)])=';' then begin result:=true;delete(sline,length(sline),1);end else exit;
  sline:=clearwhitespace(sline);
end;

function convert_dd_mm_yy_2_yyyy(sline:string):string;
//brutus - fekus, ja viem, ponahlam sa.
begin
  result:='20'+copy(sline,8,2);
end;

{$ifdef timecode}
function convert_tc_srt(sline:string):ttimecode;
var s:string;tc:TTimeCode;mi:integer;
begin
  s:=sline;
  tc.HR:=ConvertNumber(parsesmart(s,':'),0,23,0);
  tc.MN:=ConvertNumber(parsesmart(s,':'),0,59,0);
  tc.SC:=ConvertNumber(parsesmart(s,','),0,59,0);
  tc.FR:=trunc(0.025 * ConvertNumber(s,0,999,0));
  tc.FF:=0;
  result:=tc;
end;

{$endif}

function convertnumber2(sline:string;min,max,def:integer;var serr:string):integer;
var v,c: integer;
begin
result:=def;serr:='';
for v:=1 to length(sLine)do
if not(sLine[v]in['1'..'9','0','+','-'])then delete(sLine,v,1);v:=0;
val(sLine,c,v);
if v=0 then
   begin
     if (c>=min) and (c<(max+1)) then
     result:=c;
   end else begin serr:='err'+inttostr(v)+':'+sLine;end;
end;
{
function convertreal2(sline:string;min,max,def:real):real;
var v: integer;c:real;
begin
  result:=def;
  c:=Str2Float(sLine);
  if (c>=min) and (c<(max+1)) then
  result:=c
  else begin writeln('err'); result:=def; end;
end;
}
function convertboolean(sline:string;def:boolean):boolean;
var v: integer;
begin
  result:=def;
  if sline='true' then result:=true;
  if sline='1' then result:=true;
  if sline='Y' then result:=true;
  if sline='false' then result:=false;
  if sline='0' then result:=false;
  if sline='N' then result:=false;
end;

begin

end.

