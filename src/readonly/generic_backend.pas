unit Generic_Backend;
{$mode objfpc} {$H+}
interface

uses process, classes, cfgfiles, sysutils, dateutils, typecast, math;

//type TMethodPtr = procedure of object;
Type TBackendProcessCallback = Procedure(p:pointer) Of Object;


type TBackend = class
  private
    
    
  protected
    debug:byte;
    TC:TTypeCast;
  public
    
    constructor Create();
    destructor Destroy; override;
    procedure loadResponse(s:string; LIST:TStringList);
    procedure loadResponseWithCallback(s:string; LIST:TStringList; cb: TBackendProcessCallback);
    procedure launch(s:string);
    procedure LaunchTT(tt:TTypecast);
    function getTime(): Integer;
    function pidof(s:string):integer;
    function signal(pid, sig:integer):integer;
  
end;



implementation



constructor TBackend.Create();
begin
  //inherited Create;
  debug:=0;
  TC:=TTypeCast.Create();
end;

destructor TBackend.Destroy();
begin
  TC.Free();
  //writeln('CLEARED');
  inherited Destroy();
  //writeln('DESTROYED'); 
end;

procedure TBackend.loadResponse(s:string; LIST:TStringList);
var AProcess:TProcess;
var i:integer;
begin
  AProcess:=TProcess.Create(nil);
  AProcess.CommandLine := s;
  AProcess.Options := AProcess.Options + [poUsePipes, poWaitOnExit, poStderrToOutPut];
  try
    AProcess.Execute();
    //Sleep(500); // if poWaitOnExit not working as expected
    LIST.LoadFromStream(AProcess.Output);
  finally
    AProcess.Free();
  end;
  if debug>5 then 
  begin
    writeln(s);
    writeln();
    for i:=0 to LIST.count-1 do writeln(LIST[i]);
  end;
end;

procedure TBackend.loadResponseWithCallback(s:string; LIST:TStringList; cb:TBackendProcessCallback);
var AProcess:TProcess;
var cwd:string;
    CharBuffer: array [0..511] of char;
    //InputCharBuffer: array [0..511] of char;
    ReadCount: integer;
    ExitCode: integer;
    outp: string;
BEGIN
  //ExitCode := -1; //Start out with failure, let's see later if it works
  AProcess := TProcess.Create(nil); //Create a new process
  try
    //cwd:=GetCurrentDir();
    //Writeln ('Current Directory is : ',cwd);
      
    AProcess.Options := [poUsePipes, poStderrToOutPut]; //, poWaitOnExit, poStderrToOutPut Use pipes to redirect program stdin,stdout,stderr
    AProcess.CommandLine := 'stdbuf -o 0 -e 0 '+s;
    AProcess.PipeBufferSize := 5;
    AProcess.Execute;
    writeln('Proc.Running ', AProcess.Running, ' ', AProcess.Output.NumBytesAvailable);
    outp:='';
    while AProcess.Running OR ((AProcess.Output.NumBytesAvailable > 0)) do
    begin
      if AProcess.Output.NumBytesAvailable > 0 then
      begin
        ReadCount := Min(1, AProcess.Output.NumBytesAvailable); //Read up to buffer, not more
        AProcess.Output.Read(CharBuffer, ReadCount);
        outp:=outp+Copy(CharBuffer, 0, ReadCount);
        if ord(CharBuffer[0]) = 10 then 
        begin
          LIST.add(outp);
          //Write(stdOut,outp);//Flush(Output);
          TBackendProcessCallback(cb)(pointer(pchar(outp)));
          outp:='';
        end;
      end;
    end;
    if outp<>'' then
    begin
      LIST.add(outp);
      //Write(stdOut,outp);Flush(Output);
      TBackendProcessCallback(cb)(pointer(pchar(outp)));
      outp:='';
    end;
    writeln('Proc.Running ', AProcess.Running);
    ExitCode := AProcess.ExitStatus;
  finally
    AProcess.Free;
  end;
END;

procedure TBackend.Launch(s:string);
var
  Process: TProcess;
  I: Integer;
begin
  Process := TProcess.Create(nil);
  try
    Process.InheritHandles := False;
    Process.Options := [];
    Process.ShowWindow := swoShow;
 
    // Copy default environment variables including DISPLAY variable for GUI application to work
    for I := 1 to GetEnvironmentVariableCount do
      Process.Environment.Add(GetEnvironmentString(I));
 
    //Process.Executable := s;
    Process.CommandLine := s;  
    Process.Execute;
  finally
    Process.Free;
  end;
end;

procedure TBackend.LaunchTT(tt:TTypecast);
var
  Process: TProcess;
  I: Integer;
  ROW:TTypeCast;
begin
  if not TT['exec'].isString() then exit;
  Process := TProcess.Create(nil);
  try
    Process.InheritHandles := False;
    Process.Options := [];
    Process.ShowWindow := swoShow;
 
    // Copy default environment variables including DISPLAY variable for GUI application to work
    for I := 1 to GetEnvironmentVariableCount do
      Process.Environment.Add(GetEnvironmentString(I));
 
    Process.Executable := TT['exec']._str_;
    
    if TT['params'].isSet() then
    begin
      TT['params'].reset(ROW);
      while TT['params'].each(ROW) do
      begin
        Process.Parameters.add(ROW._str_);
      end;
    
    end;
      
    Process.Execute;
  finally
    Process.Free;
    writeln('freee');
  end;
end;

function TBackend.GetTime(): Integer;
  //var
  //theTime : TDateTime;
  //Hour, Min, Sec, MSec: Word;
begin
  result:=DateTimeToUnix(now());
  //theTime:= time();
  //DecodeTime(theTime,Hour, Min, Sec, MSec);
  //Result:= (Hour * 3600) + (Min * 60) + (Sec * 1);
end;

function TBackend.pidof(s:string):integer;
var SL:TStringList;
var i,pid,mypid:integer;
var ln,ss:string;
begin
  mypid:=system.getProcessID();
  result:=0;
  SL:=TStringList.create();
  loadResponse('pidof "'+s+'"', SL);
  for i:=0 to SL.count-1 do
  begin
    ln:=SL[0];
    while true do begin
      ss:=parsesmarter(ln, ' ');
      if ss<>'' then
      begin
        pid:=strtoint(ss);
        if (pid=mypid) then begin
          //write('my pid: ');
        end else begin
          result:=pid;
          SL.free();
          exit;
        end;      
      end;
      if ln='' then break;
    end; 
  end;
  SL.free();
end;

function TBackend.signal(pid,sig:integer):integer;
var SL:TStringList;
var i:integer;
begin
  result:=0;
  SL:=TStringList.create();
  loadResponse('kill -s '+inttostr(sig)+' '+inttostr(pid)+'', SL);
  for i:=0 to SL.count-1 do
  begin
    //writeln(SL[0]);
  end;
  SL.free();
end;


end.
