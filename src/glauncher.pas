program glauncher;

{$mode objfpc} {$H+}

uses Glib2, Gdk2, Gtk2, Gtk2ext, gdk2pixbuf,
gui_elements,
strings, process, classes, sysutils, baseUnix,
cfgfiles, inifiles,
typecast, eventqueue, 
generic_backend;

const version:string='0.6';
const s_data:pgchar='nope';
const compiledate:string={$INCLUDE %DATE%};
const cpu:string={$INCLUDE %FPCTARGETCPU%};
const os:string={$INCLUDE %FPCTARGETOS%};


var path,name,iconpath:string;
var error1:pGError;
var LastActivityTime, TimeOutSeconds:integer;

  shown:gboolean;
  
  GUI:TGuiElements;
  CFG:TTypeCast;
  EQ:TEventQueue;
  BACKEND:TBackend;
var execs:array [0..1000] of char;
var done:boolean;

procedure sighandler(dummy: LongInt); cdecl;
begin
  done:=true;
  //writeln();
  //writeln('stop');
  //writeln();
end;

procedure the_thread(p:gpointer);cdecl;
var TT:TTypeCast=nil;
begin
  while not done do
  begin
    //write('.');
    sleep(1000);
    if (TimeoutSeconds>0) and ((LastActivityTime+TimeoutSeconds)<BACKEND.getTime()) then done:=true;
  end;
  writeln('ending...');
  gtk_main_quit;
end;

function show_action(widget:PGtkWidget;event:PGdkEvent;data:gpointer):gboolean; cdecl;
var TAG:TTypeCast;
begin
  LastActivityTime:=BACKEND.getTime();
  TAG:=GUI.getStruct();
  //shown:=gtk_window_is_active(GTK_WINDOW(TAG['window']['el']._ptr_));
  if shown then
    begin
      gtk_window_iconify(GTK_WINDOW(TAG['window']['el']._ptr_));
      if not CFG['skip-taskbar-hint']._bool_ then
        gtk_window_set_skip_taskbar_hint(GTK_WINDOW(TAG['window']['el']._ptr_), true);
      shown:=false;    
    end else begin
      gtk_window_deiconify(GTK_WINDOW(TAG['window']['el']._ptr_));
      if not CFG['skip-taskbar-hint']._bool_ then
        gtk_window_set_skip_taskbar_hint(GTK_WINDOW(TAG['window']['el']._ptr_), false);
      gtk_window_present(GTK_WINDOW(TAG['window']['el']._ptr_));
      shown:=true;
    end;
  result:= TRUE;
end;



function keypress_action(widget:PGtkWidget;event:PGdkEventKey;data:gpointer):gboolean;
var ROW:TTypeCast=nil;
var cha:string;
begin
  result:=false;
  if (event^.state and GDK_SHIFT_MASK)>0 then exit;
  if (event^.state and GDK_CONTROL_MASK)>0 then exit;
  if (event^.state and GDK_MOD1_MASK)>0 then exit;
  if (event^.keyval>300) then exit;
  cha:=lowercase(chr(event^.keyval));
  //writeln( event^.keyval, ' ', cha );
  
  CFG['buttons'].reset(ROW);
  while CFG['buttons'].each(ROW) do
  begin
    if ROW['key'].isString() and ROW['exec'].isString() then
    begin
      if ROW['key']._str_=cha then
      begin
        LastActivityTime:=BACKEND.getTime();
        BACKEND.launch('/bin/sh -c '+ROW['exec']._str_+' &');
        show_Action(nil,nil,nil);
        break;      
      end;
    end;
  
  end;

  result:= false;  
end;


function launch_action(widget:PGtkButton;data:gpointer):gboolean; cdecl;
begin
  //writeln('click ', gtk_button_get_label(widget));
  //writeln('exec ', pgchar(data));
  LastActivityTime:=BACKEND.getTime();
  BACKEND.launch('/bin/sh -c '+strpas(data)+' &');
  show_Action(nil,nil,nil);
  result:= FALSE;
end;

procedure sigusr1handler(dummy: LongInt); cdecl;
begin
  //writeln();
  //writeln('signal USR1 !');
  //writeln();
  show_Action(nil,nil,nil);
end;

procedure setConfigDefaults();
begin

  if not CFG['vertical'].isInteger() then CFG['vertical']._int_:=1;
  if not CFG['horizontal'].isInteger() then CFG['horizontal']._int_:=5;
  if not CFG['width'].isInteger() then CFG['width']._int_:=600;
  if not CFG['height'].isInteger() then CFG['height']._int_:=300;
  if not CFG['icon-path'].isString() then CFG['icon-path']._str_:='\/home\/andy\/Pascal\/glauncher\/20220601\/glauncher\/icons\/';
  if not CFG['icon'].isString() then CFG['icon']._str_:='dot-circle-o.svg';
  if not CFG['tooltip'].isString() then CFG['tooltip']._str_:='Gnomic!';
  if not CFG['title'].isString() then CFG['title']._str_:='The Gnomic Gnome!';
  if not CFG['timeout-minutes'].isInteger() then CFG['timeout-minutes']._int_:=10;
  if not CFG['fullscreen'].isBoolean() then CFG['fullscreen']._bool_:=TRUE;
  if not CFG['maximize'].isBoolean() then CFG['maximize']._bool_:=FALSE;
  if not CFG['skip-taskbar-hint'].isBoolean() then CFG['skip-taskbar-hint']._bool_:=TRUE;
  if not CFG['buttons'].isSet() then
  begin
    CFG['buttons'][0]['sign']._str_:='_Terminal';
    CFG['buttons'][0]['icon']._str_:='terminal.svg';
    CFG['buttons'][0]['exec']._str_:='xterm';

    CFG['buttons'][1]['sign']._str_:='_File Manager';
    CFG['buttons'][1]['icon']._str_:='folder.svg';
    CFG['buttons'][1]['exec']._str_:='worker';

    CFG['buttons'][2]['sign']._str_:='_Web';
    CFG['buttons'][2]['icon']._str_:='chrome.svg';
    CFG['buttons'][2]['exec']._str_:='chromium';

    CFG['buttons'][3]['sign']._str_:='_Calculator';
    CFG['buttons'][3]['icon']._str_:='calculator.svg';
    CFG['buttons'][3]['exec']._str_:='xcalc';
    
    CFG['buttons'][4]['sign']._str_:='_Shutdown';
    CFG['buttons'][4]['icon']._str_:='power-off.svg';
    CFG['buttons'][4]['exec']._str_:='gshutdown';
  
  end;

end;

procedure define_main_window();
var TAG:TTypeCast;
var i,j,ibtn,pos,L,V,H:integer;
var p:pointer;
begin
  
  V:=CFG['vertical']._int_;
  H:=CFG['horizontal']._int_;
  iconpath:=CFG['icon-path']._str_;
  TAG:=GUI.getStruct();
  
  TAG['icon']['type']._str_:='trayicon';
  TAG['icon']['file']._str_:=iconpath+CFG['icon']._str_;
  TAG['icon']['tooltip']._str_:=CFG['tooltip']._str_;
  TAG['icon']['events']['click']['func']._ptr_:=@show_action;
  
  TAG['window']['type']._str_:='window';
  //TAG['window']['title']._str_:='Launcher (slit4run v'+version+')';
  TAG['window']['title']._str_:=CFG['title']._str_;
  if CFG['fullscreen']._bool_ or CFG['maximize']._bool_ then
    TAG['window']['resizable']._bool_:=false;
  TAG['window']['width']._int_:=CFG['width']._int_;
  TAG['window']['height']._int_:=CFG['height']._int_;
  TAG['window']['border-width']._int_:=1;
  TAG['window']['events']['close']['func']._ptr_:=@show_action;
  TAG['window']['events']['keypress']['func']._ptr_:=@keypress_action;
  TAG['window']['iconify']._bool_:=true;
  TAG['window']['skip-taskbar-hint']._bool_:=CFG['skip-taskbar-hint']._bool_;
  //TAG['window']['decorated']._bool_:=false;
  
  TAG['box0']['type']._str_:='box';
  TAG['box0']['orientation']._str_:='vertical';
  TAG['box0']['spacing']._int_:=0;
  TAG['box0']['inside']._str_:='window';

  TAG['notebook']['type']._str_:='notebook';
  TAG['notebook']['spacing']._int_:=0;
  TAG['notebook']['inside']._str_:='box0';

  TAG['box0basic']['type']._str_:='box';
  TAG['box0basic']['orientation']._str_:='vertical';
  TAG['box0basic']['spacing']._int_:=0;
  //TAG['box0basic']['inside']._str_:='box0';

  TAG['box0extended']['type']._str_:='box';
  TAG['box0extended']['orientation']._str_:='vertical';
  TAG['box0extended']['spacing']._int_:=0;
  //TAG['box0extended']['inside']._str_:='box0';
  
  TAG['caption1']['type']._str_:='caption';
  TAG['caption1']['label']._str_:=name+' v'+version+'' + chr(10) + '(c) Andy Bezak 2019 - 2022'+ chr(10) +'Compiled: '+compiledate+ chr(10) +'Target: '+os+' '+cpu;
  TAG['caption1']['spacing']._int_:=0;
  TAG['caption1']['inside']._str_:='box0extended';
  
  
  ibtn:=0;pos:=0;p:=@execs;
  for i:=1 to V do
  begin
    
    TAG['box'+inttostr(i)]['type']._str_:='box';
    TAG['box'+inttostr(i)]['orientation']._str_:='horizontal';
    TAG['box'+inttostr(i)]['height']._int_:=100;
    TAG['box'+inttostr(i)]['spacing']._int_:=0;
    TAG['box'+inttostr(i)]['inside']._str_:='box0basic';
    for j:=1 to H do
    begin    
      if CFG['buttons'][ibtn].isSet() then
      begin
        TAG['button'+inttostr(ibtn)]['type']._str_:='button';
        TAG['button'+inttostr(ibtn)]['label']._str_:=' '+CFG['buttons'][ibtn]['sign']._str_;
        TAG['button'+inttostr(ibtn)]['width']._int_:=100;
        TAG['button'+inttostr(ibtn)]['height']._int_:=100;
        TAG['button'+inttostr(ibtn)]['image']['filename']._str_:=iconpath+CFG['buttons'][ibtn]['icon']._str_;
        TAG['button'+inttostr(ibtn)]['image']['only']._bool_:=true;
        TAG['button'+inttostr(ibtn)]['image']['width']._int_:=130;
        TAG['button'+inttostr(ibtn)]['image']['height']._int_:=130;
        
        TAG['button'+inttostr(ibtn)]['spacing']._int_:=0;
        TAG['button'+inttostr(ibtn)]['inside']._str_:='box'+inttostr(i);
        TAG['button'+inttostr(ibtn)]['events']['click']['func']._ptr_:=@launch_action;
        L:=Length(CFG['buttons'][ibtn]['exec']._str_)+1;
        TAG['button'+inttostr(ibtn)]['events']['click']['data-size']._int_:=L;
        getmem(p, L);//:=pointer(@p[ibtn]);
        //fillbyte(p^,0);
        strpcopy(p, CFG['buttons'][ibtn]['exec']._str_);
        TAG['button'+inttostr(ibtn)]['events']['click']['data']._ptr_:=p;
        //writeln('??',pchar(TAG['button'+inttostr(ibtn)]['events']['click']['userdata']._ptr_^));
        
      end;
      inc(ibtn);
    end;
  end;

  GUI.build(GUI.getStruct());
 
  TAG['notebook']['items'][0]['label']._str_:='me_nu';
  TAG['notebook']['items'][0]['el']._ptr_:=TAG['box0basic']['el']._ptr_;
  TAG['notebook']['items'][1]['label']._str_:='_about';
  TAG['notebook']['items'][1]['el']._ptr_:=TAG['box0extended']['el']._ptr_;
    
  GUI.NotebookAppendPages(TAG['notebook'], TAG['notebook']['items']);
  
  for i:=0 to 100 do
  begin
    if TAG['button'+inttostr(i)].isSet() then
    begin 
      //writeln('button'+inttostr(i));
      //writeln('??',pchar(TAG['button'+inttostr(i)]['events']['click']['data']._ptr_));
      freemem(TAG['button'+inttostr(i)]['events']['click']['data']._ptr_, TAG['button'+inttostr(ibtn)]['events']['click']['data-size']._int_);
    end;
  end;
  
  //gtk_widget_hide(GTK_WIDGET(TAG['window']['el']._ptr_));
  //
  shown:=gtk_window_is_active(GTK_WINDOW(TAG['window']['el']._ptr_));
  show_Action(nil,nil,nil);
  if (CFG['fullscreen']._bool_) then
  begin
    gtk_window_fullscreen(GTK_WINDOW(TAG['window']['el']._ptr_));
  end else if (CFG['maximize']._bool_) then begin
    gtk_window_maximize(GTK_WINDOW(TAG['window']['el']._ptr_));
  end else begin
    if CFG['w'].isInteger() and CFG['h'].isInteger() then
      gtk_window_resize(Gtk_Window(TAG['window']['el']._ptr_), CFG['w']._int_, CFG['h']._int_);

    if CFG['x'].isInteger() and CFG['y'].isInteger() then
      gtk_window_move(Gtk_Window(TAG['window']['el']._ptr_), CFG['x']._int_, CFG['y']._int_);

  end;
  //gtk_window_set_resizable (Gtk_Window(TAG['window']['el']._ptr_), FALSE);

end;

var thepid,i,sig,x,y,w,h,c1,c2:integer; 
var config:string;
var configSave:boolean;
begin
  
  path:=extractfilepath(paramstr(0));
  name:=extractfilename(paramstr(0));
  writeln(name+' v'+version);
  
  BACKEND:=TBackend.create();
  LastActivityTime:=BACKEND.getTime();
  thepid:=BACKEND.pidof(name);
  if thepid<>0 then 
  begin
    sig:=SIGUSR1;
    for i:=0 to paramCount do
    begin
      //writeln(i,' ',paramstr(i));
      if paramstr(i)='--quit' then sig:=SIGTERM;
    end;
    BACKEND.signal(thepid, sig);
    BACKEND.free();exit;
  end;
  CFG:=TTypecast.Create();
  ConfigSave:=false;
  config:=GetUserDir()+'.config/'+name+'/'+name+'.json';
  //writeln(path+'../src/launcher.json');
  if fileexists(config) then
    typecast_from_json(CFG, config);
  //CFG.dump();
  c1:=CFG.Count();
  setConfigDefaults();

  
  
  TimeOutSeconds:=CFG['timeout-minutes']._int_*60;

  done:=false;
  fpSignal(SIGINT, SignalHandler(@sighandler));
  fpSignal(SIGTERM, SignalHandler(@sighandler));
  fpSignal(SIGUSR1, SignalHandler(@sigusr1handler));
   
  g_thread_init(Nil);
  gdk_threads_init();

  gtk_init (@argc, @argv);

  EQ:=TEventQueue.Create();

  GUI:=TGUIElements.Create();
  define_main_window();

  
  
  
  //loadIniFile();

  if (g_thread_create(TGThreadFunc(@the_thread), gpointer(@EQ), FALSE, @error1) = nil) then
  begin
    writeln('f...');
    exit;
  end;

  gdk_threads_enter();
  gtk_main();
  gdk_threads_leave();
  
  BACKEND.free();

  EQ.free();
 
  
  if not directoryexists(GetUserDir()+'.config') then
    MkDir((GetUserDir()+'.config'));
  if not directoryexists(GetUserDir()+'.config/'+name+'') then
    MkDir((GetUserDir()+'.config/'+name+''));

  if not (CFG['fullscreen']._bool_ or CFG['maximize']._bool_) then
  begin
    gtk_window_get_position(Gtk_Window(GUI.getStruct()['window']['el']._ptr_), @x, @y);
    CFG['x']._int_:=x;CFG['y']._int_:=y;
    gtk_window_get_size(Gtk_Window(GUI.getStruct()['window']['el']._ptr_), @w, @h);
    CFG['w']._int_:=w;CFG['h']._int_:=h;
    ConfigSave:=true;
  end else begin
    c2:=CFG.Count();
    if c1<>c2 then ConfigSave:=true;
  end;
  
  if ConfigSave then
  begin
    typecast_save_to_file(CFG,GetUserDir()+'.config/'+name+'/'+name+'.json');
  end;
  
  CFG.free();

  writeln('BYE');
    
end.
