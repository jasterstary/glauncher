#!/bin/bash
########################################################################################
COMPILE=(
)

READONLY=(
)
########################################################################################
function whereami() {
  #HOME=$(cd ~ && pwd)
  DIR=$(cd $(dirname "$0") && pwd)
  VERSION=$(basename $(dirname "$DIR"))
  #NAME=$(basename $(dirname "$DIR"))
  NAME=$(basename $(dirname $(dirname "$DIR")))
  DEVEL=$(dirname $(dirname "$DIR"))
}

function whattodo(){
  if [ -f "$DIR/compile.txt" ]; then
    mapfile -t COMPILE < "$DIR/compile.txt"
  fi
  if [ -f "$DIR/readonly.txt" ]; then
    mapfile -t READONLY < "$DIR/readonly.txt"
  fi
}

function whereareunits() {
  if [ -f "$DIR/freezed.txt" ]; then
    return 0;
  fi
  UNITS="$DEVEL/UNITS"
  if [ -d "$UNITS" ]; then
    echo "found $UNITS"
  else
    echo "mkdir $UNITS"
    mkdir "$UNITS"
  fi
}

function prepare_bin() {
  BIN="$DIR/bin"
  if [ -d "$BIN" ]; then
    if [ "$(ls -A $BIN)" ]; then
      echo "Removing old executables from $BIN"
      rm "$BIN"/*
    else
      echo "$BIN is Empty"
    fi
  else
    mkdir "$BIN"
  fi
}

function prepare_build() {
  BUILD="$DIR/build"
  if [ -d "$BUILD" ]; then
    if [ "$(ls -A $BUILD)" ]; then
      echo "Removing garbage from $BUILD"
      rm "$BUILD"/*
    else
      echo "$BUILD is Empty"
    fi
  else
    mkdir "$BUILD"
  fi
}

function prepare_src() {
  SRC="$DIR/src"
  if [ -d "$SRC" ]; then
    ls "$SRC"
  else
    echo "Directory $SRC not found, nothing to do..."
    exit
  fi
}

function register_src() {
  if [ -f "$DIR/freezed.txt" ]; then
    return 0;
  fi
  rm "$UNITS/$NAME"
  ln -s "$SRC" "$UNITS/$NAME"
}

function prepare_srcro() {
  SRCRO="$DIR/src/readonly"
  if [ -f "$DIR/freezed.txt" ]; then
    return 0;
  fi
  if [ -d "$SRCRO" ]; then
    if [ "$(ls -A $SRCRO)" ]; then
      echo "Removing old units from $SRCRO"
      rm "$SRCRO"/*
    else
      echo "$SRCRO is Empty"
    fi
  else
    mkdir "$SRCRO"
  fi

  echo "Theese files are maintained elsewhere, please do not edit them here." > "$SRCRO/README.txt"

  for i in ${READONLY[@]}; do
    echo "Copying $i to src/readonly."
    if [ -f "$UNITS/$i" ]; then
      u=$(basename "$i")
      cp "$UNITS/$i" "$SRCRO/$u"
    fi
  done
}

function compile(){
  
  #$(find /usr/lib -name "libcurl.so")
  
  cd "$BUILD"
  echo "" > build.log
  echo "" > error.log

  for i in ${COMPILE[@]}; do
    echo "BUILDING $i ...";
    echo "BUILDING $i ..." >> build.log;
    echo "BUILDING $i ..." >> error.log;
    if [ -f "$SRC/$i" ]; then
      fpc -Sd -Fu"$SRC" -Fu"$SRCRO" -FE"$BIN" -FU"$BUILD" -fPIC "$SRC/$i" >> build.log 2>> error.log 
    fi
  done
}

function postbuild(){
  if [ -f "$DIR/postbuild.sh" ]; then
    source "$DIR/postbuild.sh"
  fi
}

function pack(){
  if [ -d "/var/www/html/packages/" ]; then
    echo "$DIR/ $NAME"
    tar -zcf "$DIR/../../$NAME.tar.gz" -C "$DIR/.." .
    cp "$DIR/../../$NAME.tar.gz" "/var/www/html/packages/"
  fi
}

whereami
whattodo
whereareunits

echo "$HOME"
echo "$DIR"
echo "$VERSION"
echo "$NAME"
echo "$DEVEL"
echo "$UNITS"

prepare_src
register_src
prepare_srcro
prepare_bin
prepare_build
compile
postbuild
pack

exit


#RESULT=`fpc -Sd -Fu"$SRC" -FE./ $MAIN`
#  > build.log 2> error.log 
#echo "$RESULT"

#if [ -e "$BIN/$MAIN" ]
#then
#  echo "SUCCESS";
#else
#  echo "COMPILATION FAILED";
#fi;
