# glauncher

Simple launcher for lightweight window manager FluxBox, written in FreePascal, using GTK2.

![screenshot](https://gitlab.com/jasterstary/glauncher/-/raw/master/screenshot-001.png)

It allows to define the most used programs and add the shortcuts
to launch programs, when launcher window is active.

After the first run, the configuration file is created in path:

> ~/.config/glauncher/glauncher.json


You can edit this file, while preserving JSON syntax.
The example of configuration:

```
{
  "vertical" : 4,
  "horizontal" : 5,
  "width" : 600,
  "height" : 300,
  "icon-path" : "\/usr\/local\/share\/icons\/font-awesome\/white\/svg\/",
  "icon" : "dot-circle-o.svg",
  "tooltip" : "Gnomic!",
  "title" : "The Gnomic Gnome!",
  "timeout-minutes" : 1,
  "fullscreen" : true,
  "maximize" : false,
  "skip-taskbar-hint" : false,
  "buttons" : [
    {
      "sign" : "_Terminal",
      "icon" : "terminal.svg",
      "exec" : "xterm",
      "key": "t"
    },
    {
      "sign" : "_File Manager",
      "icon" : "folder-open-o.svg",
      "exec" : "worker",
      "key": "f"
    },
    {
      "sign" : "_Editor",
      "icon" : "edit.svg",
      "exec" : "geany",
      "key": "e"
    },
    {
      "sign" : "_Web",
      "icon" : "chrome.svg",
      "exec" : "chromium",
      "key": "w"
    },
    {
      "sign" : "_Upload",
      "icon" : "server.svg",
      "exec" : "filezilla",
      "key": "u"
    },
    {
      "sign" : "_Calculator",
      "icon" : "calculator.svg",
      "exec" : "xcalc",
      "key": "c"
    },
    {
      "sign" : "Calendar",
      "icon" : "calendar.svg",
      "exec" : "worker"
    },
    {
      "sign" : "_Keys",
      "icon" : "lock.svg",
      "exec" : "keepass2",
      "key": "k"
    },
    {
      "sign" : "_Dashboard",
      "icon" : "dashboard.svg",
      "exec" : "xterm",
      "key": "d"
    },
    {
      "sign" : "Database",
      "icon" : "database.svg",
      "exec" : "xterm"
    },
    {
      "sign" : "_Library",
      "icon" : "file-pdf-o.svg",
      "exec" : "evince",
      "key": "L"
    },
    {
      "sign" : "_Office",
      "icon" : "file-excel-o.svg",
      "exec" : "soffice",
      "key": "o"
    },
    {
      "sign" : "_Images",
      "icon" : "file-image-o.svg",
      "exec" : "geeqie",
      "key": "i"
    },
    {
      "sign" : "_Movies",
      "icon" : "file-video-o.svg",
      "exec" : "worker",
      "key": "m"
    },
    {
      "sign" : "_Zipped",
      "icon" : "file-zip-o.svg",
      "exec" : "xarchiver",
      "key": "z"
    },
    {
      "sign" : "_Vector Graphic",
      "icon" : "tint.svg",
      "exec" : "inkscape",
      "key": "v"
    },
    {
      "sign" : "Bitmap _Graphic",
      "icon" : "paint-brush.svg",
      "exec" : "gimp",
      "key": "g"
    },
    {
      "sign" : "Music _Player",
      "icon" : "volume-up.svg",
      "exec" : "audacious",
      "key": "p"
    },
    {
      "sign" : "_Recording",
      "icon" : "microphone.svg",
      "exec" : "audacity",
      "key": "r"
    },
    {
      "sign" : "_Shutdown",
      "icon" : "power-off.svg",
      "exec" : "gshutdown",
      "key": "s"
    }
  ],
  "x" : 61,
  "y" : 23,
  "w" : 911,
  "h" : 520
}

```

### Configuration Parameters:


| parameter | description |
| --- | --- |
| "vertical" | number, means count of rows. |
| "horizontal" | number, means count of columns. |
| "width" | number, minimal width of window in pixels. |
| "height" | number, minimal height of window in pixels. |
| "icon-path" | string, path to SVG icons. |
| "icon" | string, name of icon, which represents this program in tray. |
| "tooltip" | string, |
| "title" | string, title of window. |
| "timeout-minutes" | number, if greater then 0, this represents count of minutes, after which the program is stoped. |
| "fullscreen" | boolean, if true, active window is in fullscreen mode. |
| "maximize" | boolean, if true, active window is in maximized mode. |
| "skip-taskbar-hint" | boolean, if true, then program is hidden from taskbar. |
| "x", "y", "w", "h" | Don't edit. Window size and position, saved when the program is switched off. |
| "buttons" | array of launch-buttons. Each button has following string parameters: |

  
### Button Configuration Parameters:


| parameter | example | description |
| --- | ---  | --- |
|     "sign" | "_Terminal" | text on the button with mnemonic. |
|     "icon" | "terminal.svg" | icon on the button. |
|     "exec" | "xterm" | command line, which is launched by button. |
|     "key" | "t" | pressing this key, button is launched. |


### Building:

Prerequisities:
- linux
- fpc compiler 3.0.0
- gtk 2.+

Run script ./build.sh - this will create folders ./build and ./bin.
If folder ./bin remains empty, check the file ./build/build.log for errors.
